package filter

import (
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context"
)

func CorsHandler() beego.FilterFunc {
	return func(ctx *context.Context) {
		ctx.Output.Header("Access-Control-Allow-Methods", "*")
	}
}
