package filter

import (
	"net/http"
	"os"
	"xmock/app/mock"
	"xmock/cores"

	"github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context"
)

/*
*
mock handler
*/
func MockHandler() web.FilterFunc {
	return func(ctx *context.Context) {
		var errResponse cores.Entity
		code, err := mock.RenderMock(ctx)
		if code > 0 {
			if os.IsNotExist(err) {
				ctx.Output.Status = http.StatusNotFound
				ctx.Output.JSON(*errResponse.WithCode(http.StatusNotFound).WithMsg("执行脚本异常：" + err.Error()), false, false)
				return
			}
		}
	}
}
