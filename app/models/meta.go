package models

import (
	"database/sql"
	"time"

	"github.com/beego/beego/v2/client/orm"
)

// 查询 mysql 的表结构
type Schema struct {
	CatalogName             string         `orm:"column:CATALOG_NAME"`
	SchemaName              string         `orm:"column:SCHEMA_NAME"`
	DefaultCharacterSetName string         `orm:"column:DEFAULT_CHARACTER_SET_NAME"`
	DefaultCollationName    string         `orm:"column:DEFAULT_COLLATION_NAME"`
	SqlPath                 sql.NullString `orm:"column:SQL_PATH"`
}

type Table struct {
	TableCatalog   string         `orm:"column:TABLE_CATALOG"`
	TableSchema    string         `orm:"column:TABLE_SCHEMA"`
	TableName      string         `orm:"column:TABLE_NAME"`
	TableType      string         `orm:"column:TABLE_TYPE"`
	ENGINE         sql.NullString `orm:"column:ENGINE"`
	VERSION        sql.NullInt64  `orm:"column:VERSION"`
	RowFormat      sql.NullString `orm:"column:ROW_FORMAT"`
	TableRows      sql.NullInt64  `orm:"column:TABLE_ROWS"`
	AvgRowLength   sql.NullInt64  `orm:"column:AVG_ROW_LENGTH"`
	DataLength     sql.NullInt64  `orm:"column:DATA_LENGTH"`
	MaxDataLength  sql.NullInt64  `orm:"column:MAX_DATA_LENGTH"`
	IndexLength    sql.NullInt64  `orm:"column:INDEX_LENGTH"`
	DataFree       sql.NullInt64  `orm:"column:DATA_FREE"`
	AutoIncrement  sql.NullInt64  `orm:"column:AUTO_INCREMENT"`
	CreateTime     sql.NullTime   `orm:"column:CREATE_TIME"`
	UpdateTime     sql.NullTime   `orm:"column:UPDATE_TIME"`
	CheckTime      sql.NullTime   `orm:"column:CHECK_TIME"`
	TableCollation sql.NullString `orm:"column:TABLE_COLLATION"`
	CHECKSUM       sql.NullInt64  `orm:"column:CHECKSUM"`
	CreateOptions  sql.NullString `orm:"column:CREATE_OPTIONS"`
	TableComment   string         `orm:"column:TABLE_COMMENT"`
}

type Column struct {
	TableCatalog           string         `orm:"column:TABLE_CATALOG"`
	TableSchema            string         `orm:"column:TABLE_SCHEMA"`
	TableName              string         `orm:"column:TABLE_NAME"`
	ColumnName             string         `orm:"column:COLUMN_NAME"`
	OrdinalPosition        int            `orm:"column:ORDINAL_POSITION"`
	ColumnDefault          sql.NullString `orm:"column:COLUMN_DEFAULT"`
	IsNullable             string         `orm:"column:IS_NULLABLE"`
	DataType               string         `orm:"column:DATA_TYPE"`
	CharacterMaximumLength sql.NullInt64  `orm:"column:CHARACTER_MAXIMUM_LENGTH"`
	CharacterOctetLength   sql.NullInt64  `orm:"column:CHARACTER_OCTET_LENGTH"`
	NumericPrecision       sql.NullInt64  `orm:"column:NUMERIC_PRECISION"`
	NumericScale           sql.NullInt64  `orm:"column:NUMERIC_SCALE"`
	DatetimePrecision      sql.NullInt64  `orm:"column:DATETIME_PRECISION"`
	CharacterSetName       sql.NullString `orm:"column:CHARACTER_SET_NAME"`
	CollationName          sql.NullString `orm:"column:COLLATION_NAME"`
	ColumnType             string         `orm:"column:COLUMN_TYPE"`
	ColumnKey              string         `orm:"column:COLUMN_KEY"`
	EXTRA                  string         `orm:"column:EXTRA"`
	PRIVILEGES             string         `orm:"column:PRIVILEGES"`
	ColumnComment          string         `orm:"column:COLUMN_COMMENT"`
	GenerationExpression   string         `orm:"column:GENERATION_EXPRESSION"`
}

// 暂时不使用
type View struct {
	TableCatalog        string `orm:"column:TABLE_CATALOG"`
	TableSchema         string `orm:"column:TABLE_SCHEMA"`
	TableName           string `orm:"column:TABLE_NAME"`
	ViewDefinition      string `orm:"column:VIEW_DEFINITION"`
	CheckOption         string `orm:"column:CHECK_OPTION"`
	IsUpdatable         string `orm:"column:IS_UPDATABLE"`
	DEFINER             string `orm:"column:DEFINER"`
	SecurityType        string `orm:"column:SECURITY_TYPE"`
	CharacterSetClient  string `orm:"column:CHARACTER_SET_CLIENT"`
	CollationConnection string `orm:"column:COLLATION_CONNECTION"`
}

// xmock自己存放 会将mysql表结构同步到自己存放对象中
type ObjectTable struct {
	Catalog    string
	Schema     string
	Name       string
	Source     string // mysql 默认是:mysql
	TableType  string
	CreateTime time.Time
	UpdateTime time.Time
	Comment    string
}

func init() {
	orm.RegisterModel(new(ObjectTable))
}

type ObjectColumn struct {
	Catalog         string
	Schema          string
	TName           string //表名
	ColumnName      string
	ColumnDefault   string
	IsNullable      string
	DataType        string
	DataTypeComment string // 说明数据类型 如 数字，字符
	Comment         string
}

func init() {
	orm.RegisterModel(new(ObjectColumn))
}
