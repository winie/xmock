package auth

import (
	"fmt"
	"xmock/cores"

	"github.com/beego/beego/v2/core/logs"
)

func RoleDistributorResourceService(roleId int64, p *RoleDistributorResource) (responseEntity cores.ResponseEntity) {
	response := DeleteRoleResourceService(roleId)
	if response.Code == 100016 {
		return *responseEntity.BuildError(cores.BuildEntity(RoleDistributorResourceError, GetMsg(RoleDistributorResourceError)))

	}

	var roleResources []SysRoleResource
	var roleResource SysRoleResource
	for _, value := range p.ResourceId {
		roleResource.RoleId = roleId
		if value != 0 {
			roleResource.ResourceId = value
			roleResources = append(roleResources, roleResource)
		}
	}
	_, err := CreateMultiRoleResource(roleResources)
	var hateoas cores.Hateoas
	var links cores.Links
	links.Add(cores.LinkTo("/v1/role/"+fmt.Sprint(roleId)+"/resource", "self", "GET", "根据编码获取角色信息"))
	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(RoleDistributorResourceError, GetMsg(RoleDistributorResourceError)))
	}
	return *responseEntity.BuildPostAndPut(hateoas.AddLinks(links))

}

func DeleteRoleResourceService(id int64) cores.Entity {
	num, err := DeleteRoleResource(id)
	logs.Trace(111, err)
	if num < 0 && err != nil {
		return *cores.BuildEntity(DeleteRoleResourceError, GetMsg(DeleteRoleResourceError))
	} else {
		return *cores.BuildEntity(Success, GetMsg(Success))
	}
}

func ResourceByRoleIdService(roleId int64) (responseEntity cores.ResponseEntity) {
	resources, _, err := FindResourceByRoleId(roleId)
	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(QueryError, GetMsg(QueryError)+err.Error()))
	}
	var hateoas cores.HateoasTemplate
	var links cores.Links
	links.Add(cores.LinkTo("/v1/resource/{code}", "related", "GET", "根据编码获取资源信息"))
	hateoas.AddLinks(links)
	type data struct {
		Resources interface{}
		cores.HateoasTemplate
	}
	d := &data{resources, hateoas}

	return *responseEntity.Build(d)

}
