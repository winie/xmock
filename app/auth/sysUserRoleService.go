package auth

import (
	"fmt"
	"xmock/cores"
)

func UserDistributorRoleService(userId int64, p *UserDistributorRole) (responseEntity cores.ResponseEntity) {

	response := DeleteUserRoleService(userId)
	if response.Code == 100018 {
		return *responseEntity.BuildError(cores.BuildEntity(UserDistributorRoleError, GetMsg(UserDistributorRoleError)))
	}
	sysUserRole := []SysUserRole{}
	userRole := SysUserRole{}
	for _, value := range p.RoleId {
		userRole.UserId = userId
		userRole.RoleId = value
		sysUserRole = append(sysUserRole, userRole)
	}
	_, err := CreateMultiUserRole(sysUserRole)

	var hateoas cores.Hateoas
	var links cores.Links
	links.Add(cores.LinkTo("/v1/user/"+fmt.Sprint(userId)+"/role", "self", "GET", "根据ID获取角色信息"))

	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(UserDistributorRoleError, GetMsg(UserDistributorRoleError)))
	}

	return *responseEntity.BuildPostAndPut(hateoas.AddLinks(links))

}

func DeleteUserRoleService(id int64) cores.Entity {
	num, err := DeleteUserRole(id)
	if num < 0 && err != nil {
		return *cores.BuildEntity(DeleteUserRoleError, GetMsg(DeleteUserRoleError))
	} else {
		return *cores.BuildEntity(Success, GetMsg(Success))
	}
}

func RoleByUserIdService(userId int64) (responseEntity cores.ResponseEntity) {
	roles, _, err := FindRoleByUserId(userId)
	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(QueryError, GetMsg(QueryError)+err.Error()))
	}
	var hateoas cores.HateoasTemplate
	var links cores.Links
	links.Add(cores.LinkTo("/v1/role/{code}", "related", "GET", "根据编码获取信息"))
	hateoas.AddLinks(links)
	type data struct {
		Roles interface{}
		cores.HateoasTemplate
	}
	d := &data{roles, hateoas}

	return *responseEntity.Build(d)

}
