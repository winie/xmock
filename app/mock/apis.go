package mock

import (
	"fmt"
	"reflect"
	"time"
	"xmock/cores/utils"

	"github.com/beego/beego/v2/client/orm"
	beego "github.com/beego/beego/v2/server/web"
)

// mock的apis
type Apis struct {
	Id           int64     `orm:"pk;column(id);" json:"Id,string"`
	Code         string    `json:"code"`
	Name         string    `json:"name"`
	Action       string    `json:"action"`
	Method       string    `json:"method"`
	DeleteStatus int64     `json:"deleteStatus"`
	Created      time.Time `json:"created"`
	Updated      time.Time `json:"updated"`
	Proto        string    `json:"proto"`  // 配置协议:lua,template,file
	Script       string    `json:"script"` // 配置脚本
}

func init() {
	orm.RegisterModel(new(Apis))
}

type ApiOps struct {
	O orm.Ormer
}

var mockApi []*Apis

func NewApiOps() *ApiOps {
	o := orm.NewOrm()
	return &ApiOps{O: o}
}

func (o *ApiOps) QueryAllApis() (ops []*Apis) {
	ao := o.AllApis()
	if reflect.DeepEqual(ao, mockApi) {
		return
	} else {
		return ao
	}
}

// 查询所有的 mocker 接口
func (o *ApiOps) AllApis() (ops []*Apis) {
	err := utils.GetCache("AllApis", ops)
	if err != nil {
		qs := o.O.QueryTable(new(Apis)).Filter("delete_status", 0)
		qs.All(&ops)
		cache_expire, _ := beego.AppConfig.Int("cache_expire")
		utils.SetCache("AllApis", ops, cache_expire)
	}
	return
}

func (o *ApiOps) GetApis(id int64) (ops *Apis, err error) {
	err = utils.GetCache("GetlApis.id."+fmt.Sprintf("%d", id), ops)
	if err != nil {
		cache_expire, _ := beego.AppConfig.Int("cache_expire")
		ops = &Apis{Id: id}
		err = o.O.Read(ops)
		utils.SetCache("GetlApis.id."+fmt.Sprintf("%d", id), ops, cache_expire)
	}
	return ops, err
}
