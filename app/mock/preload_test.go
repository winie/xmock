package mock

import (
	"fmt"
	"testing"

	libs "github.com/vadv/gopher-lua-libs"
	lua "github.com/yuin/gopher-lua"
	luar "layeh.com/gopher-luar"
)

type User struct {
	Name  string
	token string
}

func (u *User) SetToken(t string) {
	u.token = t
}

func (u *User) Token() string {
	return u.token
}

const script = `
local db = require("stream")
print("Hello from Lua, " .. u.Name .. "!")
u:SetToken("12345")
stream({100.23, -12, "42"}).foreach(print)
`

func TestPreload(t *testing.T) {
	state := lua.NewState()
	libs.Preload(state)
	//设置lua引擎的lib脚本查找路径。
	luapath := "/?.lua" + ";" + "/lua" + "/?.lua"
	lua.LuaPathDefault = luapath
	if err := state.DoFile("D:/GolandProjects/src/xmock/vendor/github.com/vadv/gopher-lua-libs/preload.lua"); err != nil {
		t.Fatalf("execute test: %s\n", err.Error())
	}
}

func TestLua(t *testing.T) {
	L := lua.NewState()
	defer L.Close()
	libs.Preload(L)
	//设置lua引擎的lib脚本查找路径。
	luapath := "/?.lua" + ";" + "/lua" + "/?.lua"
	lua.LuaPathDefault = luapath
	u := &User{
		Name: "Tim",
	}
	L.SetGlobal("u", luar.New(L, u))

	if err := L.DoString(script); err != nil {
		panic(err)
	}
	fmt.Println("Lua set your token to:", u.Token())
	// Output:
	// Hello from Lua, Tim!
	// Lua set your token to: 12345
}
