package function_test

import (
	"testing"

	"xmock/app/mock/function"

	"github.com/stretchr/testify/assert"
)

func TestMakeSlice(t *testing.T) {
	assert.Equal(t, []interface{}{6, 2, "a", "b"}, function.MakeSlice(6, 2, "a", "b"))
}
