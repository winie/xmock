package mock

import (
	"fmt"
	"xmock/cores/render"
	"xmock/cores/router"

	"github.com/beego/beego/v2/core/logs"

	"github.com/beego/beego/v2/server/web/context"
	"github.com/pkg/errors"
)

// return file path & content & status code & error
func RenderMock(ctx *context.Context) (int, error) {
	// 开始初始化trie树
	resources := NewApiOps().QueryAllApis()
	router := router.NewRouter()
	// 需要优化下 每个请求都这样初始化树 性能问题
	for _, res := range resources {
		router.AddRoute(res.Method, res.Action, res.Id)
	}
	key, routeParams := router.GetRoutePath(ctx.Request.Method, ctx.Request.URL.Path)
	logs.Info("mock router:%v,%v", key, routeParams)
	if key < 0 {
		return -1, errors.New("不是mock")
	}
	apis, _ := NewApiOps().GetApis(key)
	schema := &render.Schema{
		RouteParams: routeParams,
		RouteId:     key,
		Script:      apis.Script,
	}
	switch apis.Proto {
	case "lua":
		renderEngine, err := render.NewLuaEngine()
		if err != nil {
			return 1, err
		}
		err = renderEngine.Exec(ctx, schema)
		if err != nil {
			return 1, err
		}
	case "file":
		renderEngine, err := render.NewFileEngine()
		if err != nil {
			return 1, err
		}
		err = renderEngine.Exec(ctx, schema)
		if err != nil {
			return 1, err
		}
	default:
		return 1, errors.WithStack(errors.New(fmt.Sprintf("Invalid body proto '%s'", apis.Proto)))
	}
	return 0, nil
}
