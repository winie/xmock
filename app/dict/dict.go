package dict

import (
	"github.com/beego/beego/v2/client/orm"
)

type SysDict struct {
	Id           int64 `orm:"pk;column(id);" json:"Id,string"`
	DictId       string
	DictTypeCode string
	DictName     string
	Sort         int
	Status       int
}

func init() {
	orm.RegisterModel(new(SysDict))
}

func CreateDict(p *SysDict) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(p)
	return id, err
}

func CheckCodeIsExist(code string) (bool, error) {
	o := orm.NewOrm()
	var dict SysDict
	err := o.QueryTable("sys_dict").Filter("code", code).One(&dict)
	if err == nil {
		return true, err
	} else {
		return false, err
	}
}

func DeleteDict(p *SysDict) (num int64, err error) {
	o := orm.NewOrm()
	num, err = o.Delete(p)
	return num, err

}

func UpdateDict(p map[string]interface{}, id int64) (num int64, err error) {
	o := orm.NewOrm()
	num, err = o.QueryTable("sys_dict").Filter("id", id).Update(p)
	return num, err

}

func ReadDict(code string) (*SysDict, error) {
	o := orm.NewOrm()
	var dict SysDict
	err := o.QueryTable("sys_dict").Filter("code", code).One(&dict)
	return &dict, err
}

func ReadDictById(id int64) (*SysDict, error) {
	o := orm.NewOrm()
	var dict SysDict
	err := o.QueryTable("sys_dict").Filter("id", id).One(&dict)
	return &dict, err
}
func ReadAllDict() ([]*SysDict, error) {
	o := orm.NewOrm()
	var dict []*SysDict
	_, err := o.QueryTable("sys_dict").All(&dict)
	return dict, err
}
