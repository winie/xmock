package form

import (
	"xmock/cores/gobatis"

	"github.com/beego/beego/v2/client/orm"
)

// 表单
// 案例元信息
type FormMeta struct {
	Id          int64  `orm:"pk;column(id);" json:"Id,string"`
	Code        string // code不能重复，因为页面不能重复
	PCode       string // 父code，由于id关系会乱但是父不会
	Title       string //表单标题
	FormType    string //表单类型 0:菜单 1:非菜单 菜单会现在在菜单栏目 非菜单不会显示，如果子是菜单 那么父一定是菜单
	Content     string // 存放表单json
	Pid         string
	Author      string
	Version     string
	UpdateDate  string
	Description string //表单说明
}

func init() {
	// 需要在init中注册定义的model
	orm.RegisterModel(new(FormMeta))
}
func (c *FormMeta) Insert() (int64, error) {
	o := orm.NewOrm()
	return o.Insert(c)
}
func (c *FormMeta) Update() (int64, error) {
	o := orm.NewOrm()
	return o.Update(c)
}
func (c *FormMeta) Delete() (int64, error) {
	o := orm.NewOrm()
	return o.Delete(c)
}

type FormMetaMapper interface {
	ReadFormMeta() (ops []*FormMeta, err error)
	ReadFormMetaByPage(p *FormMeta, pageSize int, offset int) (ops []*FormMeta, err error)
	ReadFormMetaByID(id int64) (ops *FormMeta, err error)
	ReadFormMetaByCode(code string) (ops []*FormMeta, err error)
	Delete(p *FormMeta) (num int64, err error)
	Update(p map[string]interface{}, id int64) (num int64, err error)
	Create(p *FormMeta) (int64, error)
	ReadCountByPage(ops *FormMeta) (num int64, err error)
}

type FormMetaModel struct {
	orm     orm.Ormer
	goBatis *gobatis.DB
}

func NewFormMetaMapper() (FormMetaMapper, error) {
	o := orm.NewOrm()
	goBatis := gobatis.NewGoBatis(gobatis.DBType(o.Driver().Name()))
	return &FormMetaModel{orm: o, goBatis: goBatis}, nil
}

// 查询所有的表单
func (c *FormMetaModel) ReadFormMeta() (ops []*FormMeta, err error) {
	c.orm.QueryTable(new(FormMeta)).All(ops)
	return ops, nil
}
func (c *FormMetaModel) ReadCountByPage(ops *FormMeta) (num int64, err error) {
	num, err = c.orm.QueryTable(new(FormMeta)).Filter("title__icontains", ops.Title).Count()
	return num, err
}

func (c *FormMetaModel) ReadFormMetaByPage(p *FormMeta, pageSize int, offset int) (ops []*FormMeta, err error) {
	c.orm.QueryTable(new(FormMeta)).Filter("title__icontains", p.Title).Limit(pageSize, offset).All(ops)
	return ops, nil
}
func (c *FormMetaModel) ReadFormMetaByID(id int64) (ops *FormMeta, err error) {
	c.orm.QueryTable(new(FormMeta)).Filter("Id", id).One(ops)
	return ops, nil
}
func (c *FormMetaModel) ReadFormMetaByCode(code string) (ops []*FormMeta, err error) {
	c.orm.QueryTable(new(FormMeta)).Filter("Code", code).All(ops)
	return ops, nil
}

func (c *FormMetaModel) Delete(p *FormMeta) (num int64, err error) {
	num, err = c.orm.Delete(p)
	return num, err
}
func (c *FormMetaModel) Update(p map[string]interface{}, id int64) (num int64, err error) {
	num, err = c.orm.QueryTable(new(FormMeta)).Filter("id", id).Update(p)
	return num, err
}
func (c *FormMetaModel) Create(p *FormMeta) (int64, error) {
	id, err := c.orm.Insert(p)
	return id, err
}
