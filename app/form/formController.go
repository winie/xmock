package form

import (
	"encoding/json"
	"xmock/controllers"

	"github.com/beego/beego/v2/core/validation"

	"github.com/beego/beego/v2/core/logs"

	"github.com/beego/beego/v2/core/utils/pagination"
)

type FormController struct {
	controllers.BaseController
	formMetaService FormMetaService
}

func (c *FormController) InitService() {
	formMetaService, err := NewFormMetaService()
	if err != nil {
		logs.Error("初始化表单异常:%v", err)
	}
	c.formMetaService = formMetaService
}

// 创建表单
func (c *FormController) Create() {
	c.InitService()
	var ops *FormMeta
	json.Unmarshal(c.Ctx.Input.RequestBody, ops)
	valid := validation.Validation{}
	valid.Required(ops.Id, "id").Message("不存在表单")
	valid.Required(ops.Code, "Code").Message("不存在表单")
	if valid.HasErrors() {
		c.Data["json"] = valid.Errors
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	response := c.formMetaService.Create(ops)
	c.Data["json"] = response.Data
	c.Ctx.Output.Status = response.StatusCode
	c.ServeJSON()
}

// 分页查询
func (c *FormController) FindByPage() {
	c.InitService()
	pageSize, _ := c.GetInt("perPage")
	var ops *FormMeta
	json.Unmarshal(c.Ctx.Input.RequestBody, ops)
	counts := c.formMetaService.ReadCountByPage(ops)
	page := pagination.NewPaginator(c.Ctx.Request, pageSize, counts)
	response := c.formMetaService.FindByPage(ops, page)
	c.Data["json"] = response.Data
	c.Ctx.Output.Status = response.StatusCode
	c.ServeJSON()
}

// 修改数据
func (c *FormController) Update() {
	c.InitService()
	id, err := c.GetInt64(":id")
	if err != nil {
		c.Data["json"] = GetMsg(UpdateError) + err.Error()
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	var data *FormMeta
	err = json.Unmarshal(c.Ctx.Input.RequestBody, &data)
	if err != nil {
		c.Data["json"] = GetMsg(UpdateError) + err.Error()
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	valid := validation.Validation{}
	valid.Required(id, "id").Message("不存在表单")
	if valid.HasErrors() {
		c.Data["json"] = valid.Errors
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	data.Id = id
	response := c.formMetaService.Update(data)
	c.Data["json"] = response.Data
	c.Ctx.Output.Status = response.StatusCode
}

// 发布表单
func (c *FormController) Publish() {
	c.InitService()
	id, err := c.GetInt64(":id")
	if err != nil {
		c.Data["json"] = GetMsg(UpdateError) + err.Error()
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	var data *FormMeta
	err = json.Unmarshal(c.Ctx.Input.RequestBody, &data)
	if err != nil {
		c.Data["json"] = GetMsg(UpdateError) + err.Error()
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	valid := validation.Validation{}
	valid.Required(id, "id").Message("不存在表单")
	if valid.HasErrors() {
		c.Data["json"] = valid.Errors
		c.Ctx.Output.Status = UpdateError
		c.ServeJSON()
		return
	}
	data.Id = id
	response := c.formMetaService.Publish(data)
	c.Data["json"] = response.Data
	c.Ctx.Output.Status = response.StatusCode
}
