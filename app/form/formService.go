package form

import (
	"xmock/app/auth"
	"xmock/cores"
	"xmock/cores/utils"

	"github.com/beego/beego/v2/core/utils/pagination"
)

type FormMetaService interface {
	Create(p *FormMeta) (responseEntity cores.ResponseEntity)
	ReadCountByPage(f *FormMeta) int64
	FindByPage(f *FormMeta, p *pagination.Paginator) (responseEntity cores.ResponseEntity)
	Update(data *FormMeta) (responseEntity cores.ResponseEntity)
	Publish(data *FormMeta) (responseEntity cores.ResponseEntity)
}

type FormMetaServiceImp struct {
	formMetaMapper FormMetaMapper
}

func NewFormMetaService() (FormMetaService, error) {
	formMetaMapper, err := NewFormMetaMapper()
	return &FormMetaServiceImp{formMetaMapper: formMetaMapper}, err
}

func (s *FormMetaServiceImp) Create(p *FormMeta) (responseEntity cores.ResponseEntity) {
	existCode, _ := s.formMetaMapper.ReadFormMetaByCode(p.Code)
	if existCode != nil {
		return *responseEntity.BuildError(cores.BuildEntity(ParameterError, "表单已存在"))
	}
	utils.UUID.Init(1024)
	newid, _ := utils.UUID.GetId()
	p.Id = newid
	id, err := p.Insert()
	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(ParameterError, GetMsg(ParameterError)))
	} else {
		return *responseEntity.BuildPostAndPut(id)
	}
}
func (s *FormMetaServiceImp) ReadCountByPage(f *FormMeta) int64 {
	num, err := s.formMetaMapper.ReadCountByPage(f)
	if err != nil {
		return 0
	}
	return num
}
func (s *FormMetaServiceImp) FindByPage(f *FormMeta, p *pagination.Paginator) (responseEntity cores.ResponseEntity) {
	ops, err := s.formMetaMapper.ReadFormMetaByPage(f, p.PerPageNums, p.Offset())
	type data struct {
		Resources []*FormMeta
	}
	d := &data{ops}
	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(QueryError, GetMsg(QueryError)))
	}
	return *responseEntity.NewBuild(0, d)
}
func (s *FormMetaServiceImp) Update(data *FormMeta) (responseEntity cores.ResponseEntity) {
	num, err := data.Update()
	if num < 0 && err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(UpdateError, GetMsg(UpdateError)))
	}
	return *responseEntity.Build(cores.BuildEntity(0, GetMsg(Success)))
}
func (s *FormMetaServiceImp) Publish(data *FormMeta) (responseEntity cores.ResponseEntity) {
	num, err := data.Update()
	if num < 0 && err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(UpdateError, GetMsg(UpdateError)))
	}
	//判断菜单中是否存在表单,如果存在更新成最新的表单
	opt, err := s.formMetaMapper.ReadFormMetaByID(data.Id)
	if err != nil {
		return *responseEntity.BuildError(cores.BuildEntity(UpdateError, GetMsg(UpdateError)))
	}
	res, _ := auth.ReadResourceByCode(opt.Code)
	res.Code = opt.Code
	res.Name = opt.Title
	res.FormId = opt.Id
	if opt.PCode != "" {
		// 代表 他有父亲 需要查询父亲id
		pres, _ := auth.ReadResourceByCode(opt.PCode)
		if !utils.IsEmpty(res) {
			res.ParentId = pres.Id
		}
	}
	//为空代表需要insert，如果不为空代表需要修改
	if utils.IsEmpty(res) {
		res.Id = opt.Id
		res.ResType = 1
		res.Insert()
	} else {
		res.Update()
	}
	return *responseEntity.Build(cores.BuildEntity(0, GetMsg(Success)))
}
