package form

import (
	"net/http"
)

const (
	formCode           = 100000
	Unauthorized       = http.StatusUnauthorized
	Success            = formCode
	GenerateTokenError = formCode + 1
	ParameterError     = formCode + 2
	QueryError         = formCode + 3
	UpdateError        = formCode + 4
)

var Msg = map[int]string{
	Unauthorized:       "没有权限",
	GenerateTokenError: "生成token异常",
	Success:            "成功",
	ParameterError:     "参数异常",
	QueryError:         "查询异常",
	UpdateError:        "更新异常",
}

func GetMsg(code int) string {
	return Msg[code]
}
