package cron

import (
	"context"
	"fmt"
	"xmock/cores/utils"

	libs "github.com/vadv/gopher-lua-libs"
	lua "github.com/yuin/gopher-lua"

	"github.com/beego/beego/v2/client/orm"
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/task"
)

type CronScheduler struct {
	Id      int64 `orm:"pk;column(id);" json:"Id,string"`
	Name    string
	Express string
	Content string
	Status  int // 0 正常 1 挂起 2 执行中 -1 废弃
}

func init() {
	orm.RegisterModel(new(CronScheduler))
}

// 查询所有的 mocker 接口
func AllCronScheduler() (ops []CronScheduler) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CronScheduler))
	qs.All(&ops)
	return ops
}

func GetCronScheduler(id int64) (CronScheduler, error) {
	var ops CronScheduler
	var err error
	err = utils.GetCache("GetCronScheduler.id."+fmt.Sprintf("%d", id), &ops)
	if err != nil {
		cache_expire, _ := beego.AppConfig.Int("cache_expire")
		o := orm.NewOrm()
		ops = CronScheduler{Id: id}
		err = o.Read(&ops)
		utils.SetCache("GetCronScheduler.id."+fmt.Sprintf("%d", id), ops, cache_expire)
	}
	return ops, err
}

func InitCron() {
	ops := AllCronScheduler()
	StaticPath, _ := beego.AppConfig.String("StaticPath")
	for _, v := range ops {
		tk1 := task.NewTask(v.Name, v.Express, func(ctx context.Context) error {
			L := lua.NewState()
			//设置lua引擎的lib脚本查找路径。
			luapath := "/?.lua" + ";" + StaticPath + "/lua" + "/?.lua"
			lua.LuaPathDefault = luapath
			libs.Preload(L)
			defer L.Close()
			if err := L.DoString(v.Content); err != nil {
				return err
			}
			return nil
		})
		task.AddTask(v.Name, tk1)
	}
	task.StartTask()
}
