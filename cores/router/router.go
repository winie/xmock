package router

import "strings"

// type HandlerFunc int64
type Router struct {
	Roots    map[string]*node
	Handlers map[string]int64
}

var gouters *Router

func NewRouter() *Router {
	if gouters == nil {
		return &Router{
			Roots:    make(map[string]*node),
			Handlers: make(map[string]int64),
		}
	} else {
		return gouters
	}
}

// Only one * is allowed
func parsePattern(pattern string) []string {
	vs := strings.Split(pattern, "/")

	parts := make([]string, 0)
	for _, item := range vs {
		if item != "" {
			parts = append(parts, item)
			if item[0] == '*' {
				break
			}
		}
	}
	return parts
}

func (r *Router) AddRoute(method string, pattern string, handler int64) {
	parts := parsePattern(pattern)

	key := method + "-" + pattern
	_, ok := r.Roots[method]
	if !ok {
		r.Roots[method] = &node{}
	}
	r.Roots[method].insert(pattern, parts, 0)
	r.Handlers[key] = handler
}

func (r *Router) GetRoute(method string, path string) (*node, map[string]string) {
	searchParts := parsePattern(path)
	params := make(map[string]string)
	root, ok := r.Roots[method]
	if !ok {
		return nil, nil
	}
	n := root.search(searchParts, 0)
	if n != nil {
		parts := parsePattern(n.pattern)
		for index, part := range parts {
			if part[0] == ':' {
				params[part[1:]] = searchParts[index]
			}
			if part[0] == '*' && len(part) > 1 {
				params[part[1:]] = strings.Join(searchParts[index:], "/")
				break
			}
		}
		return n, params
	}
	return nil, nil
}

func (r *Router) GetRoutePath(method string, path string) (int64, map[string]interface{}) {
	searchParts := parsePattern(path)
	params := make(map[string]interface{})
	root, ok := r.Roots[method]
	if !ok {
		return -1, nil
	}
	n := root.search(searchParts, 0)
	if n != nil {
		parts := parsePattern(n.pattern)
		for index, part := range parts {
			if part[0] == ':' {
				params[part[1:]] = searchParts[index]
			}
			if part[0] == '*' && len(part) > 1 {
				params[part[1:]] = strings.Join(searchParts[index:], "/")
				break
			}
		}
		key := method + "-" + n.pattern
		return r.Handlers[key], params
	}
	return -1, nil
}
func (r *Router) GetRoutes(method string) []*node {
	root, ok := r.Roots[method]
	if !ok {
		return nil
	}
	nodes := make([]*node, 0)
	root.travel(&nodes)
	return nodes
}
