package render

import (
	"xmock/cores/lua"

	"github.com/beego/beego/v2/core/logs"

	"github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context"
	libs "github.com/vadv/gopher-lua-libs"
	l "github.com/yuin/gopher-lua"
	luar "layeh.com/gopher-luar"
)

type LuaEngine struct {
	pool *lua.LuaPool
}

func NewLuaEngine() (RenderEngine, error) {
	return &LuaEngine{}, nil
}

func (bc *LuaEngine) Exec(ctx *context.Context, schema *Schema) error {
	StaticPath, _ := web.AppConfig.String("StaticPath")
	g := &LuaSchema{
		ctx:          ctx,
		RequestQuery: ParseQuery(ctx.Request.URL.RawQuery), // the query of URL
		RouteParams:  schema.RouteParams,                   // The Params of Route
	}
	//设置lua引擎的lib脚本查找路径。
	lua.SetLuaPath(StaticPath + "/lua")
	llib := []*lua.LuaLib{
		{ExportName: "mydb", ExportFunctions: lua.CreateDataBasseLib()},
		{ExportName: "gocontext", ExportFunctions: lua.CreateGoContextLib(ctx)},
	}
	//加载自定义库,http,db
	bc.pool = lua.NewLuaPool2(100, "xmock", llib...)
	// luascript := bc.getLuaScript(strconv.FormatInt(schema.RouteId, 10), schema.Script)
	luascript := bc.getLuaScript(schema.Script, schema.Script)
	beforeRun := func(l *l.LState) {
		// 加载所有库
		libs.Preload(l)
		l.SetGlobal("_inputs", lua.SetReadOnly(l, lua.ToLuaTable2(l, ParseQuery(ctx.Request.URL.RawQuery))))
		l.SetGlobal("g", luar.New(l, g))
	}
	afterRun := func(l *l.LState) {

	}
	_, err := luascript.Call(beforeRun, afterRun)
	return err
}

func init() {
	Register("lua", NewLuaEngine)
}
func (bc *LuaEngine) getLuaScript(code string, content string) *lua.LuaScript {
	script := &lua.LuaScript{Log: bc.lualog}
	script.SetPool(bc.pool)
	script.Load(code, content)
	return script
}
func (bc *LuaEngine) lualog(str string) {
	logs.Info("lua: %v", str)
}

type LuaSchema struct {
	ctx          *context.Context
	Faker        Mock
	RequestQuery map[string]interface{} // the query of URL
	RouteParams  map[string]interface{}
	BodyMap      map[string]interface{}
	ContentType  string // 返回内容协议
}

func (L *LuaSchema) SetContentType(t string) {
	L.ContentType = t
}
func (L *LuaSchema) WriteString(content string) {
	L.ctx.WriteString(content)
}
func (L *LuaSchema) WriteJson(data interface{}) {
	L.ctx.Output.JSON(data, false, false)
}
func (L *LuaSchema) WriteJSONP(data interface{}) {
	L.ctx.Output.JSONP(data, false)
}
func (L *LuaSchema) WriteXML(data interface{}) {
	L.ctx.Output.XML(data, false)
}
func (L *LuaSchema) WriteYAML(data interface{}) {
	L.ctx.Output.YAML(data)
}
func (L *LuaSchema) WriteBody(data []byte) {
	L.ctx.Output.Body(data)
}
func (L *LuaSchema) WriteHeader(key, val string) {
	L.ctx.Output.Header(key, val)
}
