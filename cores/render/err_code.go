package render

import "github.com/beego/beego/v2/core/berror"

const moduleName = "mockRenderEngine"

// 9001 代表 mock渲染引擎
var NilMockRenderEngineAdapter = berror.DefineCode(9001001, moduleName, "MockRenderEngineAdapter", `
It means that you register MockRenderEngineAdapter adapter by pass nil.
A MockRenderEngineAdapter adapter is an instance of MockRenderEngineAdapter interface. 
`)
var UnknownAdapter = berror.DefineCode(9001002, moduleName, "MockRenderEngineAdapter", `
Unknown adapter, do you forget to register the adapter?
`)
