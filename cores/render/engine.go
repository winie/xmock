package render

import (
	"strings"

	"github.com/beego/beego/v2/server/web/context"

	"github.com/beego/beego/v2/core/berror"
)

type Schema struct {
	RouteParams map[string]interface{}
	RouteId     int64
	Script      string
}
type RenderEngine interface {
	// 执行渲染 将beego 数据下传 原因：需要获取清楚参数和写回去接口
	Exec(ctx *context.Context, schema *Schema) error
}

// Instance is a function create a new RenderEngine Instance
type Instance func() (RenderEngine, error)

var adapters = make(map[string]Instance)

// Register makes a RenderEngine adapter available by the adapter name.
// If Register is called twice with the same name or if driver is nil,
// it panics.
func Register(name string, adapter Instance) {
	if adapter == nil {
		panic(berror.Error(NilMockRenderEngineAdapter, "mockRenderEngine: Register adapter is nil").Error())
	}
	if _, ok := adapters[name]; ok {
		panic("mockRenderEngine: Register called twice for adapter " + name)
	}
	adapters[name] = adapter
}

// NewRenderEngine creates a new RenderEngine driver by adapter name string.
func NewRenderEngine(adapterName string) (adapter RenderEngine, err error) {
	instanceFunc, ok := adapters[adapterName]
	if !ok {
		err = berror.Errorf(UnknownAdapter, "RenderEngine: unknown adapter name %s (forgot to import?)", adapterName)
		return
	}
	adapter, err = instanceFunc()
	return
}

/*
 *	解析query参数
 */
func ParseQuery(queryStr string) map[string]interface{} {
	queryMap := map[string]interface{}{}
	groups := strings.Split(queryStr, "&")
	for _, group := range groups {
		arr := strings.Split(group, "=")
		key := arr[0]
		value := strings.Join(arr[1:], "=")
		if val, ok := queryMap[key]; ok {
			switch v := val.(type) {
			case string:
				queryMap[key] = []string{v, value}
			case []string:
				newValue := append(v, value)
				queryMap[key] = newValue
			}
		} else {
			queryMap[key] = value
		}
	}
	return queryMap
}
