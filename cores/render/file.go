package render

import (
	"mime"
	"path"

	"github.com/beego/beego/v2/server/web/context"

	"github.com/beego/beego/v2/server/web"
)

type FileEngine struct {
}

func NewFileEngine() (RenderEngine, error) {
	return &FileEngine{}, nil
}

func (bc *FileEngine) Exec(ctx *context.Context, schema *Schema) error {
	StaticPath, _ := web.AppConfig.String("StaticPath")
	targetFile := StaticPath + "/" + schema.Script
	contentType := mime.TypeByExtension(path.Ext(targetFile))
	ctx.Output.Download(targetFile)
	ctx.ResponseWriter.Header().Add("Content-Type", contentType)
	return nil
}

func init() {
	Register("file", NewFileEngine)
}
