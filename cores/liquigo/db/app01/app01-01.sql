drop table if exists test_sql_file;
create table test_sql_file (
  id varchar(36) not null,
  role_name varchar(50) not null comment 'role name',
  detail varchar(250) not null default 'role detail' comment 'role detail',
  app_id varchar(36) comment 'foreign key app.id',
  primary key (id) using btree,
  unique index uk_test_sql_file_name (role_name) using btree
) comment = 'test sql file';
