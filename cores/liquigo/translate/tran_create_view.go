// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	"strings"

	utils "xmock/cores/liquigo/utils"

	etree "xmock/cores/liquigo/etree"
)

// 获取创建视图的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// createViewEle *etree.Element 创建视图的Entity标签
func GetCreateViewSql(dbmsName *string, createViewEle *etree.Element) (string, error) {
	return getDefaultCreateViewSql(dbmsName, createViewEle)
}

// <createView fullDefinition="false"
// 	replaceIfExists="false"
// 	remarks="A String"
// 	viewName="v_person">
// 	select id, name from person where id > 10
// </createView>
// 【sqlite】
// create view v_test_rename_table_new -- 'comment of this view'
// as select * from test_rename_table_new;
// 【postgres,kingbase,dm】
// create or replace view v_test_rename_table_new as
// select * from test_rename_table_new;
// comment on view v_test_rename_table_new is 'comment of this view';
// 【mysql】 mysql不支持给视图添加注释
// create or replace view v_test_rename_table_new as
// select * from test_rename_table_new;
// 【oracle】
// create or replace view v_test_rename_table_new as
// select * from test_rename_table_new;
// comment on table v_test_rename_table_new is 'comment of this view';
func getDefaultCreateViewSql(dbmsName *string, createViewEle *etree.Element) (string, error) {
	viewName := createViewEle.SelectAttrValue("viewName", utils.EMPTY)
	SetPropertyValue(dbmsName, &viewName)
	remarks := createViewEle.SelectAttrValue("remarks", utils.EMPTY)
	fullDefinition := createViewEle.SelectAttrValue("fullDefinition", utils.FALSE)
	replaceIfExists := createViewEle.SelectAttrValue("replaceIfExists", utils.FALSE)

	// trim结尾的 空格、换行、Tab
	sql := strings.TrimRight(createViewEle.Text(), utils.BLANK_CHARS)
	if !strings.HasSuffix(sql, ";") {
		sql = sql + ";"
	}

	switch *dbmsName {
	case MySQL, MariaDB, TiDB:
		if fullDefinition == utils.TRUE {
			// 完整视图定义，直接返回
			return sql + utils.LF, nil
		}
		if replaceIfExists == utils.TRUE {
			// create or replace view v_test as
			viewSql := "create or replace view " + viewName + " as " + utils.LF + sql + utils.LF
			return viewSql, nil
		} else {
			// create view v_test as
			viewSql := "create view " + viewName + " as " + utils.LF + sql + utils.LF
			return viewSql, nil
		}
	case SQLite:
		if fullDefinition == utils.TRUE {
			// 完整视图定义，直接返回
			return sql + utils.LF, nil
		}
		var viewSql string
		if replaceIfExists == utils.TRUE {
			viewSql = "drop view if exists " + viewName + ";" + utils.LF
		}

		// create view v_test_rename_table_new -- 'comment of this view'
		// as select * from test_rename_table_new;
		var comment string
		if utils.EMPTY != remarks {
			comment = " -- '" + remarks + "'" + utils.LF
		}
		viewSql = viewSql + "create view " + viewName + comment + " as " + sql + utils.LF
		return viewSql, nil
	case MsSQLServer:
		if fullDefinition == utils.TRUE {
			// 完整视图定义，直接返回
			return sql + utils.LF, nil
		}
		// create view v_test_rename_table_new
		// as select * from test_rename_table_new;
		viewSql := "create or alter view " + viewName + " as " + sql + utils.LF
		return viewSql, nil
	case PostgreSQL, Kingbase, Dameng:
		// create or replace view v_test_rename_table_new as
		// select * from test_rename_table_new;
		// comment on view v_test_rename_table_new is 'comment of this view';
		var comment string
		if utils.EMPTY != remarks {
			comment = "comment on view " + viewName + " is '" + remarks + "';" + utils.LF
		}
		if fullDefinition == utils.TRUE {
			// 完整视图定义，直接返回
			return sql + utils.LF + comment, nil
		}
		if replaceIfExists == utils.TRUE {
			// 由于视图在增减查询列后会【panic: pq: 无法从视图中删除列】，所以先drop，再create
			// create or replace view v_test as
			viewSql := "drop view if exists " + viewName + ";" + utils.LF + //
				"create or replace view " + viewName + " as " + utils.LF + //
				sql + utils.LF
			return viewSql + comment, nil
		} else {
			// create view v_test as
			viewSql := "create view " + viewName + " as " + utils.LF + sql + utils.LF
			return viewSql + comment, nil
		}
	case Oracle:
		// create or replace view v_test_rename_table_new as
		// select * from test_rename_table_new;
		// comment on table v_test_rename_table_new is 'comment of this view';
		var comment string
		if utils.EMPTY != remarks {
			comment = "comment on table " + viewName + " is '" + remarks + "';" + utils.LF
		}
		if fullDefinition == utils.TRUE {
			// 完整视图定义，直接返回
			return sql + utils.LF + comment, nil
		}
		if replaceIfExists == utils.TRUE {
			// create or replace view v_test as
			viewSql := "create or replace view " + viewName + " as " + utils.LF + sql + utils.LF
			return viewSql + comment, nil
		} else {
			// create view v_test as
			viewSql := "create view " + viewName + " as " + utils.LF + sql + utils.LF
			return viewSql + comment, nil
		}
	default:
		return "-- createView: DBMS " + *dbmsName + " not implemented yet", nil
	}
}
