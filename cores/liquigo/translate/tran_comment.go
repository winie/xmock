// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	"strings"

	utils "xmock/cores/liquigo/utils"

	etree "xmock/cores/liquigo/etree"
)

// 获取changeSet的注释的sql语句
// 返回文本格式：/* {comment} */
// dbmsName *string 数据库类型名称，例如：mysql
// commentEle *etree.Element changeSet的注释XML元素
func GetCommentSql(dbmsName *string, commentEle *etree.Element) (string, error) {
	return "/* " + commentEle.Text() + " */" + utils.LF, nil
}

// 获取changeSet的注释文本
// dbmsName *string 数据库类型名称，例如：mysql
// commentEle *etree.Element changeSet的注释XML元素
func GetCommentText(dbmsName *string, commentEle *etree.Element) string {
	return strings.Trim(commentEle.Text(), utils.BLANK_CHARS)
}
