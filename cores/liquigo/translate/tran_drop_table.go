// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	utils "xmock/cores/liquigo/utils"

	etree "xmock/cores/liquigo/etree"
)

// 获取删除表的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// dropTableEle *etree.Element 删除表的Entity标签
func GetDropTableSql(dbmsName *string, dropTableEle *etree.Element) (string, error) {
	return getDefaultDropTableSql(dbmsName, dropTableEle)
	// switch *dbmsName {
	// case MySQL:
	// 	return getDefaultCreateIndexSql(dropTableEle)
	// case SQLite:
	// 	return getDefaultCreateIndexSql(dropTableEle)
	// case "h2":
	// 	return getDefaultCreateIndexSql(dropTableEle)
	// }
	// return utils.EMPTY, nil
}

// drop table dept;
// drop table dept cascade constraints;
// drop table dept cascade; -- mysql
func getDefaultDropTableSql(dbmsName *string, dropTableEle *etree.Element) (string, error) {
	cascadeConstraints := dropTableEle.SelectAttrValue("cascadeConstraints", utils.FALSE)
	tableName := dropTableEle.SelectAttrValue("tableName", utils.EMPTY)
	SetPropertyValue(dbmsName, &tableName)
	var sql string
	if cascadeConstraints == utils.TRUE {
		switch *dbmsName {
		case MySQL, MariaDB, Kingbase, PostgreSQL, TiDB:
			sql = "drop table " + tableName + " cascade;"
		case MsSQLServer, SQLite:
			sql = "drop table if exists " + tableName + ";"
		default:
			// Dameng, Oracle, H2
			sql = "drop table " + tableName + " cascade constraints;"
		}
	} else {
		sql = "drop table " + tableName + ";"
	}
	return sql, nil
}
