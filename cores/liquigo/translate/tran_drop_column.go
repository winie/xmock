// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	"strings"

	utils "xmock/cores/liquigo/utils"

	etree "xmock/cores/liquigo/etree"

	"github.com/beego/beego/v2/core/logs"
)

// 获取删除表字段的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// dropColumnEle *etree.Element 删除表字段的Entity标签
func GetDropColumnSql(dbmsName *string, dropColumnEle *etree.Element) (string, error) {
	return getDefaultDropColumnSql(dbmsName, dropColumnEle)
}

// alter table test_t04 drop column first_login_time cascade;
// sqlite数据库不支持删除表字段操作dropColumn
func getDefaultDropColumnSql(dbmsName *string, dropColumnEle *etree.Element) (string, error) {
	tableName := dropColumnEle.SelectAttrValue("tableName", utils.EMPTY)
	SetPropertyValue(dbmsName, &tableName)

	if *dbmsName == SQLite {
		logs.Error("sqlite数据库不支持删除表字段操作dropColumn: %v", tableName)
		return "-- SQLite database does not support deleting table fields" + utils.LF, nil
	}

	columnName := dropColumnEle.SelectAttrValue("columnName", utils.EMPTY)
	SetPropertyValue(dbmsName, &columnName)

	columnElements := dropColumnEle.SelectElements("column")
	columnLen := len(columnElements)
	if columnLen > utils.INT_ZERO {
		// 包含column标签，则columnName属性失效
		var builder strings.Builder
		builder.Grow(columnLen * utils.ONE_HUNDRED)
		for _, columnEle := range columnElements {
			cname := columnEle.SelectAttrValue("name", utils.EMPTY)
			SetPropertyValue(dbmsName, &cname)
			if *dbmsName == Oracle {
				utils.StringAppender(&builder, "alter table ", tableName, " drop column ", cname, " cascade constraints;", utils.LF)
			} else if *dbmsName == MsSQLServer {
				// alter table test_role drop constraint if exists dfc_test_role_update_user;
				// alter table test_role drop column if exists update_user;
				utils.StringAppender(&builder, "alter table ", tableName, " drop constraint if exists dfc_", tableName, "_", cname, utils.SEMICOLON, utils.LF)
				utils.StringAppender(&builder, "alter table ", tableName, " drop column ", cname, utils.SEMICOLON, utils.LF)
			} else {
				utils.StringAppender(&builder, "alter table ", tableName, " drop column ", cname, " cascade;", utils.LF)
			}
		}
		return builder.String(), nil
	} else {
		// 删除columnName属性指定的字段
		if *dbmsName == Oracle {
			sql := "alter table " + tableName + " drop column " + columnName + " cascade constraints;" + utils.LF
			return sql, nil
		} else if *dbmsName == MsSQLServer {
			// alter table test_role drop constraint if exists dfc_test_role_update_user;
			// alter table test_role drop column if exists update_user;
			sql := "alter table " + tableName + " drop constraint if exists dfc_" + tableName + "_" + columnName + utils.SEMICOLON + utils.LF +
				"alter table " + tableName + " drop column if exists " + columnName + utils.SEMICOLON + utils.LF
			return sql, nil
		} else {
			sql := "alter table " + tableName + " drop column " + columnName + " cascade;" + utils.LF
			return sql, nil
		}
	}
}
