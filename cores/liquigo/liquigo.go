// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package liquigo

import (
	"xmock/cores/liquigo/config"
	"xmock/cores/liquigo/handle"
)

// 程序调用
func RealTimeRun(entryXml []string, dataSourceName string, dryRun bool) error {
	config.XormLog = true
	config.DryRun = dryRun
	var dbConfig config.DB
	dbConfig.DataSourceName = dataSourceName
	dbConfig.EntryXml = entryXml
	dbConfig.DriverName = "mysql"
	dbConfig.DbmsName = "mysql"
	dbConfig.DbmsVersion = "5"
	return handle.HandleDb(&dbConfig)
}
