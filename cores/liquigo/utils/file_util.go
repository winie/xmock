// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package utils

import (
	"os"

	"github.com/beego/beego/v2/core/logs"
)

// 获取指定文件的字节数组
// @param filePath *string 文件路径
// @return ([]byte, error)
func GetFileBytes(filePath *string) ([]byte, error) {
	file, err := os.Open(*filePath)
	if err != nil {
		logs.Error(err)
		return nil, err
	}
	defer file.Close()

	fileinfo, err := file.Stat()
	if err != nil {
		logs.Error(err)
		return nil, err
	}

	filesize := fileinfo.Size()
	buffer := make([]byte, filesize)

	bytesread, err := file.Read(buffer)
	if err != nil {
		logs.Error(err)
		logs.Error(*filePath+" bytes read: ", bytesread)
		return nil, err
	}
	return buffer, nil
}
