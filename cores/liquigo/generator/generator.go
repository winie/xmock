package generator

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"os"
	"regexp"
	"strconv"

	"github.com/beego/beego/v2/core/logs"

	"github.com/xuri/excelize/v2"
)

// var author = flag.String("author", "liquibase", "Set ChangeSet author name")
// var id = flag.String("id", "1", "Set ChangeSet id")
// var sourceFile = flag.String("source", "", "Select the Excel's file with data")
// var destinationFile = flag.String("destination", fmt.Sprintf("changeset-%s.xml", *id), "Change filename of output XML file")
var author = "liquibase"
var id = "1"
var sourceFile = "examples/template.xlsx"
var destinationFile = fmt.Sprintf("changeset-%s.xml", id)

func generator() {
	// fmt.Printf("Start reading file %s...\n", *sourceFile)
	// xlsFile, err := xlsx.OpenFile(*sourceFile)
	xlsFile, err := excelize.OpenFile(sourceFile)
	if err != nil {
		logs.Error("打开文件异常%v", err)
		return
	}
	defer func() {
		if err := xlsFile.Close(); err != nil {
			logs.Error("关闭文件异常%v", err)
		}
	}()
	if err != nil {
		fmt.Printf("File reading error\n")
		os.Exit(1)
	}

	changeSet := createChangeSetFromFile(xlsFile)
	dataBaseChangeLog := createDataBaseChangeLog(changeSet)
	xml := generateXML(dataBaseChangeLog)

	writeToFile(destinationFile, changeCloseTags(xml))

}

func (table *CreateTable) addColumn(column Column) {
	table.Columns = append(table.Columns, column)
}

func createChangeSetFromFile(xlsFile *excelize.File) ChangeSet {
	var changeSet ChangeSet

	for _, sheet := range xlsFile.GetSheetList() {
		table := CreateTable{
			TableName: sheet,
		}
		rows, _ := xlsFile.GetRows(sheet)
		for i := range rows {
			// for i, colCell := range row {
			// 	colcelindex := fmt.Sprintf("%s%d", colIndex[i], j+1)
			// }
			cellName, err := xlsFile.GetCellValue(sheet, "A"+strconv.Itoa(i+2))
			if err != nil {
				logs.Error("获取字段属性错误:%v", err)
			}
			cellType, err := xlsFile.GetCellValue(sheet, "B"+strconv.Itoa(i+2))
			if err != nil {
				logs.Error("获取字段类型错误:%v", err)
			}
			cellNullable, err := xlsFile.GetCellValue(sheet, "C"+strconv.Itoa(i+2))
			if err != nil {
				logs.Error("获取字段约束:%v", err)
			}
			cellNullableBool, err := strconv.ParseBool(cellNullable)
			if err != nil {
				logs.Error("获取字段约束:%v", err)
			}
			cellRemarks, err := xlsFile.GetCellValue(sheet, "D"+strconv.Itoa(i+2))
			if err != nil {
				logs.Error("获取字段说明:%v", err)
			}
			column := Column{
				Name:    cellName,
				Type:    cellType,
				Remarks: cellRemarks,
				Constraints: Constraints{
					Nullable: cellNullableBool,
				},
			}
			table.addColumn(column)
		}
		changeSet = ChangeSet{
			Author:      author,
			ID:          id,
			CreateTable: table,
		}
	}
	return changeSet
}

func createDataBaseChangeLog(changeSet ChangeSet) DatabaseChangeLog {
	dbcl := DatabaseChangeLog{
		Xmlns:             Xmlns,
		XmlnsXsi:          XmlnsXsi,
		XsiSchemaLocation: XsiSchemaLocation,
		ChangeSet:         changeSet,
	}
	return dbcl
}

func generateXML(dbChangelog DatabaseChangeLog) string {
	xmlBytes, err := xml.MarshalIndent(dbChangelog, "", "    ")
	if err != nil {
		panic(err)
	}
	return Header + string(xmlBytes)
}

func writeToFile(fileName string, xml string) {
	f, err := os.Create(fileName)
	if err != nil {
		panic(err)
	}

	w := bufio.NewWriter(f)
	b, err := w.WriteString(xml)
	if b < len(xml) {
		if err != nil {
			panic(err)
		}
	}
	w.Flush()
}

func changeCloseTags(xml string) string {
	r := regexp.MustCompile("></[[:alnum:]]*>")
	return r.ReplaceAllString(xml, "/>")
}
