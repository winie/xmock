package liquigo

import (
	"testing"

	"github.com/beego/beego/v2/core/logs"
)

// func TestMain(m *testing.M) {
// 	logs.Info("liquigo start")
// 	if err := handle.Handle("app.yml"); err != nil {
// 		if strings.HasPrefix(err.Error(), "liquigo stopped") {
// 			logs.Info(err.Error())
// 		} else {
// 			logs.Error("%v", err)
// 		}
// 	}
// 	logs.Info("liquigo end")
// }

func TestRealTimeRun(m *testing.T) {
	logs.Info("liquigo start")
	entryXml := []string{"entry-base.xml"}
	dataSourceName := `root:root@tcp(192.168.138.235:31813)/testdb?charset=utf8mb4&multiStatements=true`
	RealTimeRun(entryXml, dataSourceName, false)
	logs.Info("liquigo end")
}
