package cores

import (
	"xmock/cores/utils"

	"github.com/beego/beego/v2/core/logs"
	"github.com/beego/beego/v2/core/validation"
	"github.com/kingzcheung/carbon"
)

var Datetime validation.CustomFunc = func(v *validation.Validation, obj interface{}, key string) {
	logs.Debug("传入的字段名称:", key)
	logs.Debug("传入的字段内容:", obj)
	logs.Debug("传入的验证:", v)
	if utils.IsEmpty(obj) {
		return
	}
	name, ok := obj.(int64) //断言为我传入的整型
	if !ok {
		v.AddError(key, "传入的字段格式不正确")
		return
	}

	//从时间戳中解析
	c := carbon.CreateFromTimestamp(name)
	if c.Year < 1971 {
		v.AddError(key, "日期内容异常") //将错误信息直接返回 配合前文我写的错误信息拼接字段名，就可以做到直接返回完整的错误提示
	}
}
var MessageTmpls = map[string]string{
	"Required":     "不能为空",
	"Min":          "最小为 %d",
	"Max":          "最大为 %d",
	"Range":        "范围在 %d 至 %d",
	"MinSize":      "最小长度为 %d",
	"MaxSize":      "最大长度为 %d",
	"Length":       "长度必须是 %d",
	"Alpha":        "必须是有效的字母字符",
	"Numeric":      "必须是有效的数字字符",
	"AlphaNumeric": "必须是有效的字母或数字字符",
	"Match":        "必须匹配格式 %s",
	"NoMatch":      "必须不匹配格式 %s",
	"AlphaDash":    "必须是有效的字母或数字或破折号(-_)字符",
	"Email":        "必须是有效的邮件地址",
	"IP":           "必须是有效的IP地址",
	"Base64":       "必须是有效的base64字符",
	"Mobile":       "必须是有效手机号码",
	"Tel":          "必须是有效电话号码",
	"Phone":        "必须是有效的电话号码或者手机号码",
	"ZipCode":      "必须是有效的邮政编码",
}

//默认设置通用的错误验证和提示项
func SetDefaultMessage() {
	if len(MessageTmpls) == 0 {
		return
	}
	//将默认的提示信息转为自定义
	for k, _ := range MessageTmpls {
		validation.MessageTmpls[k] = MessageTmpls[k]
	}
	//增加默认的自定义验证方法
	err := validation.AddCustomFunc("Datetime", Datetime)
	if err != nil {
		logs.Info("自定义验证错误：", err)
	}
}

//
//var NonNegativeInteger validation.CustomFunc = func(v *validation.Validation, obj interface{}, key string) {
//	var regu = regexp.MustCompile("^\\d+$"); //写上自己的验证内容，我为正则验证
//	name, ok := obj.(int)                    //断言为我传入的整型
//	if !ok {
//		v.AddError(key, "传入的字段格式不正确")
//		return
//	}
//	result := regu.MatchString(strconv.FormatInt(int64(name), 10))
//	if !result {
//		v.AddError(key, "必须是非负正整数") //将错误信息直接返回 配合前文我写的错误信息拼接字段名，就可以做到直接返回完整的错误提示
//	}
//}
