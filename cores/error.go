package cores

import (
	"net/http"

	beego "github.com/beego/beego/v2/server/web"
)

type ErrorController struct {
	beego.Controller
}

func (c *ErrorController) Error401() {
	c.Data["json"] = Entity{
		Code: http.StatusUnauthorized,
		Msg:  "Permission denied",
	}
	c.ServeJSON()
}
func (c *ErrorController) Error403() {
	c.Data["json"] = Entity{
		Code: http.StatusForbidden,
		Msg:  "Forbidden",
	}
	c.ServeJSON()
}
