package lua

import (
	"fmt"
	"strings"
	"xmock/cores/gobatis"

	"github.com/beego/beego/v2/client/orm"
	l "github.com/yuin/gopher-lua"
)

func CreateDataBasseLib() map[string]l.LGFunction {
	luadb := LuaDataBase{}
	luadb.Init()
	libs := make(map[string]l.LGFunction)
	libs["queryOne"] = luadb.lua_findOne
	libs["query"] = luadb.lua_find
	libs["queryByPage"] = luadb.lua_query_page
	libs["insert"] = luadb.lua_insert
	libs["update"] = luadb.lua_update
	libs["find"] = luadb.lua_find
	libs["delete"] = luadb.lua_delete
	libs["insertMap"] = luadb.insertMap
	libs["updateMap"] = luadb.updateMap
	return libs

}

/*
给lua提供的数据库相关操作
*/
type LuaDataBase struct {
	DB orm.Ormer
}

func (t *LuaDataBase) Init() {
	t.DB = orm.NewOrm()
}

/*
*

	获取单条数据
	 findOne(sql,table) (table,bool)
*/
func (t *LuaDataBase) lua_findOne(l *l.LState) int {
	parames := l.Get(-1)
	l.Pop(1)
	sql := l.Get(-1).String()
	l.Pop(1)
	p := ToGoMap(parames)

	boundSql, err := gobatis.NewGoBatis(gobatis.DBType(t.DB.Driver().Name())).SelectDynamic(sql, p)
	if err != nil {
		l.Push(ToLuaValue("sql 解析异常:" + err.Error()))
		l.Push(ToLuaValue(false))
		return 2
	}
	if _, ok := boundSql["sql"]; ok {
		l.Push(ToLuaValue("未找到sql:" + sql))
		l.Push(ToLuaValue(false))
		return 2
	}
	var result []interface{}
	err = t.DB.Raw(boundSql["sql"].SqlStr, boundSql["sql"].ActualParam...).QueryRow(&result)
	if err != nil {
		l.Push(ToLuaValue("执行sql异常" + err.Error()))
		l.Push(ToLuaValue(false))
		return 2
	} else {
		l.Push(ArrayToLuaTable(l, result))
		l.Push(ToLuaValue(true))
	}
	return 2
}

/*
*

	获取单条数据
	 find(sql,table) (table,bool)
*/
func (t *LuaDataBase) lua_find(l *l.LState) int {
	parames := l.Get(-1)
	l.Pop(1)
	sql := l.Get(-1).String()
	l.Pop(1)
	p := ToGoMap(parames)
	boundSql, err := gobatis.NewGoBatis(gobatis.DBType(t.DB.Driver().Name())).SelectDynamic(sql, p)
	if err != nil {
		l.Push(ToLuaValue("sql 解析异常:" + err.Error()))
		l.Push(ToLuaValue(false))
		return 2
	}
	if _, ok := boundSql["sql"]; ok {
		l.Push(ToLuaValue("未找到sql:" + sql))
		l.Push(ToLuaValue(false))
		return 2
	}
	var result []interface{}
	_, err = t.DB.Raw(boundSql["sql"].SqlStr, boundSql["sql"].ActualParam...).QueryRows(&result)
	if err != nil {
		l.Push(ToLuaValue("执行sql异常" + err.Error()))
		l.Push(ToLuaValue(false))
		return 2
	} else {
		l.Push(ArrayToLuaTable(l, result))
		l.Push(ToLuaValue(true))
	}
	return 2
}

/*
*

	查询指定page的数据数据
	query(sql,table,pageNo,size) (table,intid,error)
*/
func (t *LuaDataBase) lua_query_page(l *l.LState) int {
	pageSize := l.ToInt(-1)
	l.Pop(1)
	pageNo := l.ToInt(-1)
	l.Pop(1)
	parames := l.Get(-1)
	l.Pop(1)
	sql := l.Get(-1).String()
	l.Pop(1)
	p := ToGoMap(parames)
	rowbound := gobatis.RowBounds(pageNo, pageSize)
	boundSql, err := gobatis.NewGoBatis(gobatis.DBType(t.DB.Driver().Name())).SelectDynamic(sql, p, rowbound)
	if err != nil {
		l.Push(ToLuaValue(false))
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 解析异常:" + err.Error()))
		return 3
	}
	if _, ok := boundSql["sql"]; ok {
		l.Push(ToLuaValue(false))
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("未找到sql:" + sql))
		return 3
	}
	if _, ok := boundSql["cntSql"]; ok {
		l.Push(ToLuaValue(false))
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("未找到就总数sql:" + sql))
		return 3
	}
	res := make(orm.Params)
	num, err := t.DB.Raw(boundSql["cntSql"].SqlStr, boundSql["cntSql"].ActualParam...).RowsToMap(&res, "name", "value")
	if err != nil {
		l.Push(ToLuaValue(false))
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("查询总数异常:" + err.Error()))
		return 3
	}
	var result []interface{}
	_, err = t.DB.Raw(boundSql["sql"].SqlStr, boundSql["sql"].ActualParam...).QueryRows(&result)
	l.Push(ArrayToLuaTable(l, result))
	if err == nil && num > 0 {
		l.Push(ToLuaValue(res["cnt"]))
	} else {
		l.Push(ToLuaValue(-1))
	}
	l.Push(ToLuaValue(err))
	return 3
}

/*
*

	插入数据
	insert(sql,table)(intid,bool,msg)
	name:
*/
func (t *LuaDataBase) lua_insert(l *l.LState) int {
	parames := l.Get(-1)
	l.Pop(1)
	sql := l.Get(-1).String()
	l.Pop(1)
	p := ToGoMap(parames)
	boundSql, err := gobatis.NewGoBatis(gobatis.DBType(t.DB.Driver().Name())).InsertDynamic(sql, p)
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue(false))
		l.Push(ToLuaValue("sql 解析异常:" + err.Error()))
		return 3
	}
	res, err := t.DB.Raw(boundSql.SqlStr, boundSql.ActualParam...).Exec()
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue(false))
		l.Push(ToLuaValue("sql 执行异常:" + err.Error()))
		return 3
	} else {
		num, _ := res.RowsAffected()
		l.Push(ToLuaValue(num))
		l.Push(ToLuaValue(true))
		l.Push(ToLuaValue("ok"))
	}
	return 3
}

/*
*

	更新数据
	update(sql,table)(int,msg)
	name:
*/
func (t *LuaDataBase) lua_update(l *l.LState) int {
	parames := l.Get(-1)
	l.Pop(1)
	sql := l.Get(-1).String()
	l.Pop(1)
	p := ToGoMap(parames)
	boundSql, err := gobatis.NewGoBatis(gobatis.DBType(t.DB.Driver().Name())).UpdateDynamic(sql, p)
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 解析异常:" + err.Error()))
		return 2
	}
	res, err := t.DB.Raw(boundSql.SqlStr, boundSql.ActualParam...).Exec()
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 执行异常:" + err.Error()))
		return 2
	} else {
		num, _ := res.RowsAffected()
		l.Push(ToLuaValue(num))
		l.Push(ToLuaValue("ok"))
	}
	return 2
}

/*
*

	删除数据
	update(sql,table)(int,msg)
	name:
*/
func (t *LuaDataBase) lua_delete(l *l.LState) int {
	parames := l.Get(-1)
	l.Pop(1)
	sql := l.Get(-1).String()
	l.Pop(1)
	p := ToGoMap(parames)
	boundSql, err := gobatis.NewGoBatis(gobatis.DBType(t.DB.Driver().Name())).DeleteDynamic(sql, p)
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 解析异常:" + err.Error()))
		return 2
	}
	res, err := t.DB.Raw(boundSql.SqlStr, boundSql.ActualParam...).Exec()
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 执行异常:" + err.Error()))
		return 2
	} else {
		num, _ := res.RowsAffected()
		l.Push(ToLuaValue(num))
		l.Push(ToLuaValue("ok"))
	}
	return 2
}

/*
*

(table string,properties map[string]interface{}) (int64, error)

	只支持mysql 未兼容其他数据库

*
*/
func (t *LuaDataBase) insertMap(l *l.LState) int {
	parames := l.Get(-1)
	l.Pop(1)
	tableName := l.Get(-1).String()
	l.Pop(1)
	properties := ToGoMap(parames)
	var keys []string
	var placeholders []string
	var args []interface{}
	for key, val := range properties {
		keys = append(keys, key)
		placeholders = append(placeholders, "?")
		args = append(args, val)
	}
	ss := fmt.Sprintf("%v,%v", '`', '`')
	statement := fmt.Sprintf("INSERT INTO %v%v%v (%v%v%v) VALUES (%v)",
		'`',
		tableName,
		'`',
		'`',
		strings.Join(keys, ss),
		'`',
		strings.Join(placeholders, ", "))
	res, err := t.DB.Raw(statement, args...).Exec()
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 执行异常:" + err.Error()))
		return 2
	} else {
		num, _ := res.RowsAffected()
		l.Push(ToLuaValue(num))
		l.Push(ToLuaValue("ok"))
	}
	return 2
}

// (table string,properties map[string]interface{} properties map[string]interface{}) (int64, error)
func (t *LuaDataBase) updateMap(l *l.LState) int {
	where := l.Get(-1)
	l.Pop(1)
	parames := l.Get(-1)
	l.Pop(1)
	tableName := l.Get(-1).String()
	l.Pop(1)
	properties := ToGoMap(parames)
	whereMap := ToGoMap(where)
	var updates []string
	var condition []string
	var args []interface{}
	for key, val := range properties {
		updates = append(updates, fmt.Sprintf("%v%v%v = ?", '`', key, '`'))
		args = append(args, val)
	}
	for key, val := range whereMap {
		condition = append(condition, fmt.Sprintf("%v%v%v = ?", '`', key, '`'))
		args = append(args, val)
	}
	statement := fmt.Sprintf("UPDATE %v%v%v SET %v  WHERE %v", '`', tableName, '`', strings.Join(updates, ", "), strings.Join(condition, " AND "))
	res, err := t.DB.Raw(statement, args...).Exec()
	if err != nil {
		l.Push(ToLuaValue(-1))
		l.Push(ToLuaValue("sql 执行异常:" + err.Error()))
		return 2
	} else {
		num, _ := res.RowsAffected()
		l.Push(ToLuaValue(num))
		l.Push(ToLuaValue("ok"))
	}
	return 2
}
