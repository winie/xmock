package lua

import (
	"bytes"
	"encoding/json"
	"net/url"

	"github.com/beego/beego/v2/server/web/context"
	l "github.com/yuin/gopher-lua"
)

func CreateGoContextLib(ctx *context.Context) map[string]l.LGFunction {
	luac := LuaGoContext{}
	luac.Init(ctx)
	libs := make(map[string]l.LGFunction)
	libs["getString"] = luac.getString
	libs["getBodyString"] = luac.getBodyString
	libs["getBodyJsonToMap"] = luac.getBodyJsonToMap
	libs["getBodyJsonToTable"] = luac.getBodyJsonToTable
	libs["getStrings"] = luac.getStrings
	libs["writeString"] = luac.WriteString
	libs["writeJson"] = luac.WriteJson
	libs["writeHeader"] = luac.WriteHeader
	libs["writeBody"] = luac.WriteBody
	libs["writeXML"] = luac.WriteXML
	libs["JsonUnMarshal"] = luac.JsonUnMarshal
	libs["JsonMarshal"] = luac.JsonMarshal

	return libs
}

type LuaGoContext struct {
	ctx *context.Context
}

func (t *LuaGoContext) Init(ctx *context.Context) {
	t.ctx = ctx
}
func (c *LuaGoContext) getBodyString(l *l.LState) int {
	buff := bytes.NewBuffer(nil)
	buff.Read(c.ctx.Input.RequestBody)
	l.Push(ToLuaValue(buff.String()))
	return 1
}
func (c *LuaGoContext) getBodyJsonToMap(l *l.LState) int {
	jdata := map[string]interface{}{}
	c.ctx.BindJSON(&jdata)
	l.Push(ToLuaTable2(l, jdata))
	return 1
}
func (c *LuaGoContext) getBodyJsonToTable(l *l.LState) int {
	l.Push(c.unmarshal(l, c.ctx.Input.RequestBody))
	return 1
}
func (c *LuaGoContext) BindBody(l *l.LState) int {
	var data interface{}
	err := c.ctx.Bind(&data)
	if err != nil {
		l.Push(ToLuaValue("读取body异常:" + err.Error()))
		return 1
	}
	l.Push(ToLuaValue(data))
	return 1
}

//getString(string)
func (c *LuaGoContext) getString(l *l.LState) int {
	key := l.Get(-1).String()
	l.Pop(1)
	if v := c.ctx.Input.Query(key); v != "" {
		l.Push(ToLuaValue(v))
		return 1
	}
	l.Push(ToLuaValue(""))
	return 1
}

// key string []string
// GetStrings returns the input string slice by key string or the default value while it's present and input is blank
// it's designed for multi-value input field such as checkbox(input[type=checkbox]), multi-selection.
func (c *LuaGoContext) getStrings(l *l.LState) int {
	key := l.Get(-1).String()
	l.Pop(1)
	var defv []string
	if f, err := c.Input(); f == nil || err != nil {
		l.Push(StringArrayToLuaTable(l, defv))
		return 1
	} else if vs := f[key]; len(vs) > 0 {
		l.Push(StringArrayToLuaTable(l, vs))
		return 1
	}
	l.Push(StringArrayToLuaTable(l, defv))
	return 1
}

// WriteString(content)
func (c *LuaGoContext) WriteString(l *l.LState) int {
	content := l.Get(-1).String()
	l.Pop(1)
	c.ctx.WriteString(content)
	return 0
}

// WriteJson(data)
func (c *LuaGoContext) WriteJson(l *l.LState) int {
	data := l.Get(-1)
	l.Pop(1)
	opt := NewLuaOption()
	c.ctx.Output.JSON(ToGoValue(data, opt), false, false)
	return 0
}
func (c *LuaGoContext) WriteJSONP(l *l.LState) int {
	data := l.Get(-1)
	l.Pop(1)
	opt := NewLuaOption()
	c.ctx.Output.JSONP(ToGoValue(data, opt), false)
	return 0
}
func (c *LuaGoContext) WriteXML(l *l.LState) int {
	data := l.Get(-1)
	l.Pop(1)
	opt := NewLuaOption()
	c.ctx.Output.XML(ToGoValue(data, opt), false)
	return 0
}
func (c *LuaGoContext) WriteYAML(l *l.LState) int {
	data := l.Get(-1)
	l.Pop(1)
	opt := NewLuaOption()
	c.ctx.Output.YAML(ToGoValue(data, opt))
	return 0
}
func (c *LuaGoContext) WriteBody(l *l.LState) int {
	data := l.Get(-1)
	l.Pop(1)
	opt := NewLuaOption()
	c.ctx.Output.Body(ToGoValue(data, opt).([]byte))
	return 0
}

// key, val string
func (c *LuaGoContext) WriteHeader(l *l.LState) int {
	val := l.Get(-1).String()
	l.Pop(1)
	key := l.Get(-1).String()
	l.Pop(1)
	c.ctx.Output.Header(key, val)
	return 0
}

// Input returns the input data map from POST or PUT request body and query string.
func (c *LuaGoContext) Input() (url.Values, error) {
	if c.ctx.Request.Form == nil {
		err := c.ctx.Request.ParseForm()
		if err != nil {
			return nil, err
		}
	}
	return c.ctx.Request.Form, nil
}

// 检查Table是否为List
func checkList(value l.LValue) (b bool) {
	if value.Type().String() == "table" {
		b = true
		value.(*l.LTable).ForEach(func(k, v l.LValue) {
			if k.Type().String() != "number" {
				b = false
				return
			}
		})
	}
	return
}

func marshal(data l.LValue) interface{} {
	switch data.Type() {
	case l.LTTable:
		if checkList(data) {
			jdata := make([]interface{}, 0)
			data.(*l.LTable).ForEach(func(key, value l.LValue) {
				jdata = append(jdata, marshal(value))
			})
			return jdata
		} else {
			jdata := map[string]interface{}{}
			data.(*l.LTable).ForEach(func(key, value l.LValue) {
				jdata[key.String()] = marshal(value)
			})
			return jdata
		}
	case l.LTNumber:
		return float64(data.(l.LNumber))
	case l.LTString:
		return string(data.(l.LString))
	case l.LTBool:
		return bool(data.(l.LBool))
	}
	return nil
}

func (c *LuaGoContext) JsonMarshal(L *l.LState) int {
	data := L.ToTable(1)
	str, err := json.Marshal(marshal(data))
	if err != nil {
		L.Push(l.LString(err.Error()))
	}
	L.Push(l.LString(str))
	return 1
}

func (c *LuaGoContext) unmarshal(L *l.LState, data interface{}) l.LValue {
	switch data.(type) {
	case map[string]interface{}:
		tb := L.NewTable()
		for k, v := range data.(map[string]interface{}) {
			tb.RawSet(l.LString(k), c.unmarshal(L, v))
		}
		return tb
	case []interface{}:
		tb := L.NewTable()
		for i, v := range data.([]interface{}) {
			tb.Insert(i+1, c.unmarshal(L, v))
		}
		return tb
	case float64:
		return l.LNumber(data.(float64))
	case string:
		return l.LString(data.(string))
	case bool:
		return l.LBool(data.(bool))
	}
	return l.LNil
}

func (c *LuaGoContext) JsonUnMarshal(L *l.LState) int {
	str := L.ToString(1)
	jdata := map[string]interface{}{}
	err := json.Unmarshal([]byte(str), &jdata)
	if err != nil {
		L.Push(l.LString(err.Error()))
	}
	L.Push(c.unmarshal(L, jdata))
	return 1
}
