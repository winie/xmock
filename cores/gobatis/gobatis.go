package gobatis

import "fmt"

// 该库的目的就只支持sql解析，其他交给其他库解决
type GoBatis interface {
	// 动态Insert 插入数据 支持xml里面的条件语法，只返回sql
	InsertDynamic(sql string, param interface{}) (boundSql *BoundSql, err error)
	// 动态Update 更新数据 支持xml里面的条件语法，只返回sql
	UpdateDynamic(sql string, param interface{}) (boundSql *BoundSql, err error)
	// 动态Delete 刪除数据 支持xml里面的条件语法，只返回sql
	DeleteDynamic(sql string, param interface{}) (boundSql *BoundSql, err error)
	// 动态Select 查询数据 支持xml里面的条件语法，如果rowBound长度大于0，cntSql:代表分页求总数sql sql:分页sql，等于0 代表 sql:代表解析后的sql
	SelectDynamic(sql string, param interface{}, rowBound ...*rowBounds) (map[string]*BoundSql, error)
	// SelectDynamic(sql string, param interface{}) (boundSql *BoundSql, err error)
}

// DB
type DB struct {
	gbBase
}

type gbBase struct {
	dbType DBType
}

type rowBounds struct {
	offset int
	limit  int
}

func RowBounds(offset int, limit int) *rowBounds {
	if offset < 0 {
		offset = 0
	}
	if limit < 0 {
		limit = 0
	}
	return &rowBounds{
		offset: offset,
		limit:  limit,
	}
}

func NewGoBatis(dbType DBType) *DB {
	return &DB{
		gbBase{
			dbType: dbType,
		},
	}
}

// Insert 插入数据
func (g *gbBase) InsertDynamic(sql string, param interface{}) (boundSql *BoundSql, err error) {
	params := paramProcess(param)
	executor := &executor{
		gb: g,
	}
	boundSql, err = executor.insertDynamic(sql, g.dbType, params)
	return boundSql, err
}

// Update 更新数据
func (g *gbBase) UpdateDynamic(sql string, param interface{}) (boundSql *BoundSql, err error) {
	params := paramProcess(param)
	executor := &executor{
		gb: g,
	}
	boundSql, err = executor.updateDynamic(sql, g.dbType, params)
	return boundSql, err
}

// Delete 刪除数据
func (g *gbBase) DeleteDynamic(sql string, param interface{}) (boundSql *BoundSql, err error) {
	params := paramProcess(param)
	executor := &executor{
		gb: g,
	}
	boundSql, err = executor.deleteDynamic(sql, g.dbType, params)
	return boundSql, err
}

// Select 查询数据
// func (g *gbBase) SelectDynamic(sql string, param interface{}) (boundSql *BoundSql, err error) {
// 	params := paramProcess(param)
// 	executor := &executor{
// 		gb: g,
// 	}
// 	boundSql, err = executor.selectDynamic(sql, g.dbType, params, resultTypeMaps)
// 	return boundSql, err
// }

func (g *gbBase) SelectDynamic(sql string, param interface{}, rowBound ...*rowBounds) (map[string]*BoundSql, error) {
	params := paramProcess(param)
	executor := &executor{
		gb: g,
	}
	bSql := make(map[string]*BoundSql, 0)
	cntBoundSql, err := executor.selectDynamic(sql, g.dbType, params, resultTypeMaps)
	if err != nil {
		return bSql, err
	}
	if len(rowBound) > 0 {
		var cnt *BoundSql
		var cntPage *BoundSql
		// 求和sql
		countSql := "SELECT COUNT(1) cnt FROM (" + cnt.SqlStr + ") AS t"
		cnt.SqlStr = countSql
		cnt.ActualParam = cntBoundSql.ActualParam
		cnt.OriginalSql = cntBoundSql.OriginalSql
		cnt.ParamMappings = cntBoundSql.ParamMappings
		cnt.ExtParams = cntBoundSql.ExtParams
		// 求分页sql
		bSql["cntSql"] = cnt
		sql := cntBoundSql.SqlStr + " LIMIT " + fmt.Sprint(rowBound[0].limit) + " OFFSET " + fmt.Sprint(rowBound[0].offset)
		actualParam := append(cntBoundSql.ActualParam, rowBound[0].limit, rowBound[0].offset)
		cntPage.SqlStr = sql
		cntPage.ActualParam = actualParam
		cntPage.OriginalSql = cntBoundSql.OriginalSql
		cntPage.ParamMappings = cntBoundSql.ParamMappings
		cntPage.ExtParams = cntBoundSql.ExtParams
		bSql["sql"] = cntPage
	} else {
		bSql["sql"] = cntBoundSql
	}
	return bSql, err
}
