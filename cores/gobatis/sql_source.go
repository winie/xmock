package gobatis

import (
	"reflect"
	"strings"
	"unicode"

	"github.com/beego/beego/v2/core/logs"
)

// gobatis的核心, 从配置到sql, 参数映射......
type BoundSql struct {
	SqlStr        string
	ParamMappings []string
	Params        map[string]interface{}
	ExtParams     map[string]interface{}
	OriginalSql   string        //原始sql
	ActualParam   []interface{} // 变量
}

type iSqlSource interface {
	getBoundSql(isRaw bool, params map[string]interface{}) *BoundSql
}

type dynamicSqlSource struct {
	sqlNode iSqlNode
}

func (d *dynamicSqlSource) getBoundSql(isRaw bool, params map[string]interface{}) *BoundSql {
	ctx := newDynamicContext(params)
	d.sqlNode.build(ctx)

	sss := staticSqlSource{
		sqlStr: ctx.toSql(),
		isRaw:  isRaw,
	}

	bs := sss.getBoundSql(isRaw, params)
	bs.ExtParams = ctx.params

	return bs
}

type staticSqlSource struct {
	sqlStr        string //替换后的sql
	isRaw         bool   //是否原始sql
	paramMappings []string
	originalSql   string        //原始sql
	actualParam   []interface{} // 变量
}

func (ss *staticSqlSource) getBoundSql(isRaw bool, params map[string]interface{}) *BoundSql {
	ss.originalSql = ss.sqlStr
	ss.isRaw = isRaw
	// ss.dollarTokenHandler(params)
	ss.tokenHandler(params)
	// ss.colonTokenHandler(params)
	return &BoundSql{
		SqlStr:        ss.sqlStr,
		ParamMappings: ss.paramMappings,
		Params:        params,
		OriginalSql:   ss.originalSql,
		ActualParam:   ss.actualParam,
	}
}

// 静态token处理, 将#{xx} ${xx} :xx 预处理为数据库预编译语句
func (ss *staticSqlSource) tokenHandler(params map[string]interface{}) {
	sqlStr := ss.sqlStr
	// if !strings.Contains(sqlStr, "#") || !strings.Contains(sqlStr, "$") || !strings.Contains(sqlStr, ":") {
	// 	return
	// }
	finalSqlStr := ""
	itemStr := ""
	start := 0
	tokenstart := "" //token开始时间
	tokenend := false
	for i := 0; i < len(sqlStr); i++ {
		if start > 0 {
			itemStr += string(sqlStr[i])
		}

		if i != 0 && i < len(sqlStr) {
			if string([]byte{sqlStr[i-1], sqlStr[i]}) == "#{" {
				tokenstart = "#{"
				start = i
			} else if string([]byte{sqlStr[i-1], sqlStr[i]}) == "${" {
				tokenstart = "${"
				start = i
			} else if string([]byte{sqlStr[i-1], sqlStr[i]}) == ":" {
				tokenstart = ":"
				start = i
			}
		}
		if tokenstart == "#{" || tokenstart == "${" {
			tokenend = start != 0 && i < len(sqlStr)-1 && sqlStr[i+1] == '}'
		} else if tokenstart == ":" {
			tokenend = start != 0 && (i+1 == len(sqlStr) || (i < len(sqlStr)-1 && isSeparatorsChar(sqlStr[i+1])))
		}

		if start != 0 && tokenend {
			finalSqlStr += sqlStr[:start-1]
			// sqlStr = sqlStr[i+2:]
			if i+1 < len(sqlStr) {
				sqlStr = sqlStr[i+2:]
			} else {
				sqlStr = ""
			}
			itemStr = strings.Trim(itemStr, " ")
			itemStr = strings.TrimSpace(itemStr)
			param, ok := params[itemStr]
			if !ok {
				// LOG.Error("param:" + itemStr + " not exists")
				logs.Error("param: %snot exists", itemStr)
				continue
			}
			if tokenstart == "${" {
				finalSqlStr += param.(string)
			} else {
				ss.paramMappings = append(ss.paramMappings, itemStr)
				if ss.isRaw {
					psql, rparam := SqlInParam(param)
					finalSqlStr += psql
					ss.actualParam = append(ss.actualParam, rparam...)
				} else {
					finalSqlStr += " ? "
				}
			}

			i = 0
			start = 0
			itemStr = ""
			tokenstart = ""
			tokenend = false
		}
	}

	if start != 0 {
		// LOG.Warn("token not close")
		logs.Warn("token not close")
	}

	finalSqlStr += sqlStr
	finalSqlStr = strings.TrimSpace(finalSqlStr)
	ss.sqlStr = finalSqlStr
}

var PARAMETER_SEPARATORS = [19]string{`"`, `\'`, `:`, `&`, `,`, `;`, `(`, `)`, `|`, `=`, `+`, `-`, `*`, `%`, `/`, `\\`, `<`, `>`, `^`}

// If Char Is Whitespace
func isSeparatorsChar(statement byte) bool {
	if unicode.IsSpace(rune(statement)) {
		return true
	}
	for _, p := range PARAMETER_SEPARATORS {
		if p == string(statement) {
			return true
		}
	}
	return false
}

// isEmpty gets whether the specified object is considered empty or not.
func SqlInParam(object interface{}) (string, []interface{}) {
	param := make([]interface{}, 0)
	var sql string
	// get nil case out of the way
	if object == nil {
		return sql, param
	}
	listKind := reflect.TypeOf(object).Kind()
	listValue := reflect.ValueOf(object)
	if listKind == reflect.String || listKind == reflect.Int64 || listKind == reflect.Int || listKind == reflect.Int8 || listKind == reflect.Int16 || listKind == reflect.Int32 {
		sql = sql + ` ? `
		param = append(param, object)
		return sql, param
	}
	for i := 0; i < listValue.Len(); i++ {
		sql = sql + ` ? `
		if i+1 <= listValue.Len()-1 {
			sql = sql + `,`
		}
		param = append(param, listValue.Index(i).Interface())
	}
	return sql, param
}
