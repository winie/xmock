package gobatis

import (
	"log"
	"testing"
)

func TestGobatisSql(t *testing.T) {
	xmlStr := `
	UPDATE t_gap SET gap = #{gap} WHERE id = #{id}
	<if test="name != nil">
			AND name = #{name}
	</if>
    `
	gb, err := NewGoBatis(DBType("mysql")).SelectDynamic(xmlStr, map[string]interface{}{"gap": 1, "id": 1})
	if err != nil {
		log.Fatalf("cccc%v", err)
	}
	log.Fatalf("www%v", gb)
}
