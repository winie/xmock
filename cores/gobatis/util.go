package gobatis

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"time"
)

// PI64 to NullInt64
func PI64(i int64) *int64 {
	return &i
}

// PS to NullString
func PS(s string) *string {
	return &s
}

// PF64 to NullFloat64
func PF64(f float64) *float64 {
	return &f
}

// PT to NullTime
func PT(t time.Time) *time.Time {
	return &t
}

// NB to NullBool
func PB(b bool) *bool {
	return &b
}

// ToInt64 convert any numeric value to int64
func ToInt64(value interface{}) (d int64, err error) {
	val := reflect.ValueOf(value)
	switch value.(type) {
	case int, int8, int16, int32, int64:
		d = val.Int()
	case uint, uint8, uint16, uint32, uint64:
		d = int64(val.Uint())
	default:
		err = fmt.Errorf("ToInt64 need numeric not `%T`", value)
	}
	return
}

// 字串截取
func SubString(s string, pos, length int) string {
	runes := []rune(s)
	l := pos + length
	if l > len(runes) {
		l = len(runes)
	}
	return string(runes[pos:l])
}
func containsKind(kinds []reflect.Kind, kind reflect.Kind) bool {
	for i := 0; i < len(kinds); i++ {
		if kind == kinds[i] {
			return true
		}
	}

	return false
}

// isNil checks if a specified object is nil or not, without Failing.
func IsNil(object interface{}) bool {
	if object == nil {
		return true
	}

	value := reflect.ValueOf(object)
	kind := value.Kind()
	isNilableKind := containsKind(
		[]reflect.Kind{
			reflect.Chan, reflect.Func,
			reflect.Interface, reflect.Map,
			reflect.Ptr, reflect.Slice},
		kind)

	if isNilableKind && value.IsNil() {
		return true
	}

	return false
}
func IsContain(items []string, item interface{}) bool {
	ok, found := IncludeElement(items, item)
	if ok && found {
		return true
	}
	return false
}

// isEmpty gets whether the specified object is considered empty or not.
func IsEmpty(object interface{}) bool {

	// get nil case out of the way
	if object == nil {
		return true
	}

	objValue := reflect.ValueOf(object)

	switch objValue.Kind() {
	// collection types are empty when they have no element
	case reflect.Array, reflect.Chan, reflect.Map, reflect.Slice:
		return objValue.Len() == 0
		// pointers are empty if nil or if the value they point to is empty
	case reflect.Ptr:
		if objValue.IsNil() {
			return true
		}
		deref := objValue.Elem().Interface()
		return IsEmpty(deref)
		// for all other types, compare against the zero value
	default:
		zero := reflect.Zero(objValue.Type())
		return reflect.DeepEqual(object, zero.Interface())
	}
}

// containsElement try loop over the list check if the list includes the element.
// return (false, false) if impossible.
// return (true, false) if element was not found.
// return (true, true) if element was found.
func IncludeElement(list interface{}, element interface{}) (ok, found bool) {

	listValue := reflect.ValueOf(list)
	listKind := reflect.TypeOf(list).Kind()
	defer func() {
		if e := recover(); e != nil {
			ok = false
			found = false
		}
	}()

	if listKind == reflect.String {
		elementValue := reflect.ValueOf(element)
		return true, strings.Contains(listValue.String(), elementValue.String())
	}

	if listKind == reflect.Map {
		mapKeys := listValue.MapKeys()
		for i := 0; i < len(mapKeys); i++ {
			if ObjectsAreEqualValues(mapKeys[i].Interface(), element) {
				return true, true
			}
		}
		return true, false
	}

	for i := 0; i < listValue.Len(); i++ {
		if ObjectsAreEqualValues(listValue.Index(i).Interface(), element) {
			return true, true
		}
	}
	return true, false

}

// ObjectsAreEqual determines if two objects are considered equal.
//
// This function does no assertion of any kind.
func ObjectsAreEqual(expected, actual interface{}) bool {
	if expected == nil || actual == nil {
		return expected == actual
	}

	exp, ok := expected.([]byte)
	if !ok {
		return reflect.DeepEqual(expected, actual)
	}

	act, ok := actual.([]byte)
	if !ok {
		return false
	}
	if exp == nil || act == nil {
		return exp == nil && act == nil
	}
	return bytes.Equal(exp, act)
}

// ObjectsAreEqualValues gets whether two objects are equal, or if their
// values are equal.
func ObjectsAreEqualValues(expected, actual interface{}) bool {
	if ObjectsAreEqual(expected, actual) {
		return true
	}

	actualType := reflect.TypeOf(actual)
	if actualType == nil {
		return false
	}
	expectedValue := reflect.ValueOf(expected)
	if expectedValue.IsValid() && expectedValue.Type().ConvertibleTo(actualType) {
		// Attempt comparison after type conversion
		return reflect.DeepEqual(expectedValue.Convert(actualType).Interface(), actual)
	}

	return false
}

// 检察文件是否允许读
func AllowRead(path string) bool {
	_, err := os.OpenFile(path, os.O_RDONLY, 0666)
	return err == nil
}

// 检察文件是否允许写
func AllowWrite(path string) bool {
	_, err := os.OpenFile(path, os.O_WRONLY, 0666)
	return err == nil
}

// 复制文件，目标文件所在目录不存在，则创建目录后再复制
// Copy(`d:\test\hello.txt`,`c:\test\hello.txt`)
func Copy(dstFileName, srcFileName string) (w int64, err error) {
	//打开源文件
	srcFile, err := os.Open(srcFileName)
	if err != nil {
		return 0, err
	}
	defer srcFile.Close()
	// 创建新的文件作为目标文件
	dstFile, err := os.Create(dstFileName)
	if err != nil {
		//如果出错，很可能是目标目录不存在，需要先创建目标目录
		err = os.MkdirAll(filepath.Dir(dstFileName), 0666)
		if err != nil {
			return 0, err
		}
		//再次尝试创建
		dstFile, err = os.Create(dstFileName)
		if err != nil {
			return 0, err
		}
	}
	defer dstFile.Close()
	//通过bufio实现对大文件复制的自动支持
	dst := bufio.NewWriter(dstFile)
	defer dst.Flush()
	src := bufio.NewReader(srcFile)
	w, err = io.Copy(dst, src)
	if err != nil {
		return 0, err
	}
	return w, err
}

// 复制目录
func CopyDir(dst string, src string) (err error) {
	if src == "" {
		return fmt.Errorf("source directory cannot be empty")
	}
	if dst == "" {
		return fmt.Errorf("destination directory cannot be empty")
	}
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)
	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return fmt.Errorf("source is not a directory")
	}
	if !Exist(dst) {
		err = os.MkdirAll(dst, si.Mode())
		if err != nil {
			return
		}
	}
	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return
	}
	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())
		if entry.IsDir() {
			err = CopyDir(srcPath, dstPath)
			if err != nil {
				return
			}
		} else {
			// Skip symlinks.
			if entry.Mode()&os.ModeSymlink != 0 {
				continue
			}
			_, err = Copy(dstPath, srcPath)
			if err != nil {
				return
			}
		}
	}
	return
}

// 获得程序所在当前文件路径 注意末尾不包含/
// d:\test\hello.exe  ==>     d:\test
func CurrentDir() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	return strings.Replace(dir, "\\", "/", -1)
}

// 获取可执行文件的绝对路径（包括文件名）
// d:\test\hello.exe
func CurrentExePath() string {
	exeName, err := filepath.Abs(os.Args[0])
	if err != nil {
		panic(err)
	}
	return strings.Replace(exeName, "\\", "/", -1)
}

// 检察文件是或者目录否存在
func Exist(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

// 返回不包含文件路径的文件名
// c:\test\hello.txt ==> hello.txt
func FileName(path string) string {
	return filepath.Base(path)
}

// 返回文件的后缀名
// c:\test\hello.txt ==> .txt
func FileNameExt(path string) string {
	return filepath.Ext(path)
}

// 获取无后缀的文件名
// c:\test\hello.txt ==> hello
func FileNameNoExt(path string) string {
	return strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))
}

// 获取文件所在目录, 注意末尾不包含/
func FileNameDir(path string) string {
	return filepath.Dir(path)
}

// 获得文件的修改时间
func FileModTime(path string) (int64, error) {
	f, err := os.Stat(path)
	if err != nil {
		return 0, err
	}
	return f.ModTime().Unix(), nil
}

// 返回文件的大小
func FileSize(path string) (int64, error) {
	f, err := os.Stat(path)
	if err != nil {
		return 0, err
	}
	return f.Size(), nil
}

// 遍历目录及下级目录，查找符合后缀文件,如果suffix为空，则查找所有文件
// 如果后缀为空，则查找任何后缀的文件
func GetFileListBySuffix(dirPath, suffix string) (files []string, err error) {
	if !IsDir(dirPath) {
		return nil, fmt.Errorf("given path does not exist: %s", dirPath)
	}
	files = make([]string, 0, 30)
	suffix = strings.ToUpper(suffix)                                                      //忽略后缀匹配的大小写
	err = filepath.Walk(dirPath, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		if err != nil { //忽略错误
			return err
		}
		if fi.IsDir() { // 忽略目录
			return nil
		}
		if suffix == "" {
			files = append(files, filename)
		} else {
			if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
				files = append(files, filename)
			}
		}
		return nil
	})
	return files, err
}

// 遍历指定目录下的所有文件，查找符合后缀文件,不进入下一级目录搜索
// 如果后缀为空，则查找任何后缀的文件
func GetFileListJustCurrentDirBySuffix(dirPath string, suffix string) (files []string, err error) {
	if !IsDir(dirPath) {
		return nil, fmt.Errorf("given path does not exist: %s", dirPath)
	}
	files = make([]string, 0, 10)
	dir, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	PathSep := string(os.PathSeparator)
	suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	for _, fi := range dir {
		if fi.IsDir() { // 忽略目录
			continue
		}
		if suffix == "" {
			files = append(files, dirPath+PathSep+fi.Name())
		} else {
			if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
				files = append(files, dirPath+PathSep+fi.Name())
			}
		}
	}
	return files, nil
}

// 把文件大小转换成人更加容易看懂的文本
// HumaneFileSize calculates the file size and generate user-friendly string.
func HumaneFileSize(s uint64) string {
	logn := func(n, b float64) float64 {
		return math.Log(n) / math.Log(b)
	}
	humanateBytes := func(s uint64, base float64, sizes []string) string {
		if s < 10 {
			return fmt.Sprintf("%dB", s)
		}
		e := math.Floor(logn(float64(s), base))
		suffix := sizes[int(e)]
		val := float64(s) / math.Pow(base, math.Floor(e))
		f := "%.0f"
		if val < 10 {
			f = "%.1f"
		}
		return fmt.Sprintf(f+"%s", val, suffix)
	}
	sizes := []string{"B", "KB", "MB", "GB", "TB", "PB", "EB"}
	return humanateBytes(s, 1024, sizes)
}

// 判断是否是目录
func IsDir(path string) bool {
	f, e := os.Stat(path)
	if e != nil {
		return false
	}
	return f.IsDir()
}

// 返回是否是文件
func IsFile(path string) bool {
	f, e := os.Stat(path)
	if e != nil {
		return false
	}
	return !f.IsDir()
}

// 打开指定文件，并从指定位置写入数据
func WriteAt(path string, b []byte, off int64) error {
	file, err := os.OpenFile(path, os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.WriteAt(b, off)
	return err
}

// 打开指定文件,并在文件末尾写入数据
func WriteAppend(path string, b []byte) error {
	file, err := os.OpenFile(path, os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.Write(b)
	return err
}
