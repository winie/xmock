package gobatis

import (
	"bytes"
	"strings"
	"xmock/cores/utils"

	"github.com/beego/beego/v2/core/logs"
)

type executor struct {
	gb *gbBase
}

func (exec *executor) updateDynamic(sql string, dbType DBType, params map[string]interface{}) (boundSql *BoundSql, err error) {
	utils.UUID.Init(1024)
	id, _ := utils.UUID.GetId()
	boundSql, err = dynamicSqlProc(string(rune(id)), "update", sql, dbType, params, "")
	if nil != err {
		return nil, err
	}
	return boundSql, err
}
func (exec *executor) insertDynamic(sql string, dbType DBType, params map[string]interface{}) (boundSql *BoundSql, err error) {
	utils.UUID.Init(1024)
	id, _ := utils.UUID.GetId()
	boundSql, err = dynamicSqlProc(string(rune(id)), "insert", sql, dbType, params, "")
	if nil != err {
		return nil, err
	}
	return boundSql, err
}

func (exec *executor) deleteDynamic(sql string, dbType DBType, params map[string]interface{}) (boundSql *BoundSql, err error) {
	utils.UUID.Init(1024)
	id, _ := utils.UUID.GetId()
	boundSql, err = dynamicSqlProc(string(rune(id)), "delete", sql, dbType, params, "")
	if nil != err {
		return nil, err
	}
	return boundSql, err
}

func (exec *executor) selectDynamic(sql string, dbType DBType, params map[string]interface{}, resultType ResultType) (boundSql *BoundSql, err error) {
	utils.UUID.Init(1024)
	id, _ := utils.UUID.GetId()
	boundSql, err = dynamicSqlProc(string(rune(id)), "select", sql, dbType, params, resultType)
	if nil != err {
		return nil, err
	}
	return boundSql, err
}

func dynamicSqlProc(id, sqlType, sql string, dbType DBType, params map[string]interface{}, resultType ResultType) (boundSql *BoundSql, err error) {
	var xmlStr bytes.Buffer
	xmlStr.WriteString(`<?xml version="1.0" encoding="utf-8"?> <mapper namespace="DynamicSqlMapper">`)
	if sqlType == "select" {
		xmlStr.WriteString(`<select id="` + id + `" resultType="` + string(resultType) + `"> ` + sql + ` </select>`)
	} else if sqlType == "insert" {
		xmlStr.WriteString(`<insert id="` + id + `"> ` + sql + ` </insert>`)
	} else if sqlType == "update" {
		xmlStr.WriteString(`<update id="` + id + `"> ` + sql + ` </update>`)
	} else if sqlType == "delete" {
		xmlStr.WriteString(`<delete id="` + id + `"> ` + sql + ` </delete>`)
	} else if sqlType == "sql" {
		xmlStr.WriteString(`<sql id="` + id + `"> ` + sql + ` </sql>`)
	} else {
		logs.Error("不支持类型")
		return
	}
	xmlStr.WriteString(`</mapper>`)
	r := strings.NewReader(xmlStr.String())
	conf := buildMapperConfig(r)
	mappedStmt := conf.getMappedStmt("DynamicSqlMapper." + id)
	mappedStmt.dbType = dbType
	boundSql = mappedStmt.sqlSource.getBoundSql(true, params)
	logs.Info("SQL:%s\nParamMappings:%s\nParams:%v", boundSql.SqlStr, boundSql.ParamMappings, boundSql.ActualParam)
	return
}
