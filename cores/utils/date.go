package utils

import (
	"strconv"
	"time"
)

/*
func GetDate(timestamp int64) string {
	tm := time.Unix(timestamp, 0)
	return tm.Format("2006-01-02 15:04")
}
func GetDateMH(timestamp int64) string {
	tm := time.Unix(timestamp, 0)
	return tm.Format("01-02 03:04")
}*/

func GetDateFormat(timestamp int64, format string) string {
	if timestamp <= 0 {
		return ""
	}
	tm := time.Unix(timestamp, 0)
	return tm.Format(format)
}

func GetDate(timestamp int64) string {
	if timestamp <= 0 {
		return ""
	}
	tm := time.Unix(timestamp, 0)
	return tm.Format("2006-01-02 15:04:00")
}

func GetDateMH(timestamp int64) string {
	if timestamp <= 0 {
		return ""
	}
	tm := time.Unix(timestamp, 0)
	return tm.Format("2006-01-02 15:04")
}

func GetTimeParse(times string) int64 {
	if "" == times {
		return 0
	}
	loc, _ := time.LoadLocation("Local")
	parse, _ := time.ParseInLocation("2006-01-02 15:04", times, loc)
	return parse.Unix()
}

func GetDateParse(dates string) int64 {
	if "" == dates {
		return 0
	}
	loc, _ := time.LoadLocation("Local")
	parse, _ := time.ParseInLocation("2006-01-02", dates, loc)
	return parse.Unix()
}

func GetDateTimeParse(times string) int64 {
	if "" == times {
		return 0
	}
	loc, _ := time.LoadLocation("Local")
	parse, _ := time.ParseInLocation("2006-01-02 15:04:01", times, loc)
	return parse.Unix()
}

// IsUnixTime check if string is valid unix timestamp value
func IsUnixTime(str string) bool {
	if _, err := strconv.Atoi(str); err == nil {
		return true
	}
	return false
}

type TimeS struct {
	timeStr string
	option  string
	num     int64
}

//时间戳与时间的互相转换
//@param            interface         int64与string格式的数据  前者转换格式化时间 后者转换时间戳
//@param            option         时间戳转换的格式
//@return            args        格式化时间
func (ts *TimeS) GetTimeStrOrStamp(param interface{}, option string) (interface{}, error) {
	//根据值的类型不同赋予不同的字段
	ts.option = option
	var result interface{}
	var err error
	switch param.(type) {
	case int64:
		p := param.(int64)
		ts.num = p
		result = ts.GetStampToFormat()
	case string:
		p := param.(string)
		ts.timeStr = p
		result, err = ts.GetFormatToStamp()
	default:
		p := param.(int64)
		ts.num = p
		result = ts.GetStampToFormat()
	}
	return result, err
}

//时间转换时间戳
//@param            num         时间
//@param            option         时间戳转换的格式
//@return            args        格式化时间
func (ts *TimeS) GetFormatToStamp() (int64, error) {
	var timeStr string
	timeStr = ts.timeStr
	option := ts.option
	loc, _ := time.LoadLocation("Asia/Shanghai") //设置时区
	switch option {
	case "YmdHis":
		tt, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr, loc)
		return tt.Unix(), err
	case "YmdHi":
		tt, err := time.ParseInLocation("2006-01-02 15:04", timeStr, loc)
		return tt.Unix(), err
	case "Ymd":
		tt, err := time.ParseInLocation("2006-01-02", timeStr, loc)
		return tt.Unix(), err
	case "ANSIC":
		tt, err := time.ParseInLocation("Mon Jan _2 15:04:05 2006", timeStr, loc)
		return tt.Unix(), err
	case "UnixDate":
		tt, err := time.ParseInLocation("Mon Jan _2 15:04:05 MST 2006", timeStr, loc)
		return tt.Unix(), err
	case "RFC822Z":
		tt, err := time.ParseInLocation("02 Jan 06 15:04 -0700", timeStr, loc)
		return tt.Unix(), err
	case "RFC850":
		tt, err := time.ParseInLocation("Monday, 02-Jan-06 15:04:05 MST", timeStr, loc)
		return tt.Unix(), err
	case "RFC1123":
		tt, err := time.ParseInLocation("Mon, 02 Jan 2006 15:04:05 MST", timeStr, loc)
		return tt.Unix(), err
	case "RFC1123Z":
		tt, err := time.ParseInLocation("Mon, 02 Jan 2006 15:04:05 -0700", timeStr, loc)
		return tt.Unix(), err
	case "RFC3339":
		tt, err := time.ParseInLocation("2006-01-02T15:04:05Z07:00", timeStr, loc)
		return tt.Unix(), err
	case "RFC3339Nano":
		tt, err := time.ParseInLocation("2006-01-02T15:04:05.999999999Z07:00", timeStr, loc)
		return tt.Unix(), err
	case "Kitchen":
		tt, err := time.ParseInLocation("3:04PM", timeStr, loc)
		return tt.Unix(), err
	case "Stamp":
		tt, err := time.ParseInLocation("Jan _2 15:04:05", timeStr, loc)
		return tt.Unix(), err
	case "StampMilli":
		tt, err := time.ParseInLocation("Jan _2 15:04:05.000", timeStr, loc)
		return tt.Unix(), err
	case "StampMicro":
		tt, err := time.ParseInLocation("Jan _2 15:04:05.000000", timeStr, loc)
		return tt.Unix(), err
	case "StampNano":
		tt, err := time.ParseInLocation("Jan _2 15:04:05.000000000", timeStr, loc)
		return tt.Unix(), err
	default:
		tt, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr, loc)
		return tt.Unix(), err
	}

}

func (ts *TimeS) GetStampToFormat() string {
	num := ts.num
	option := ts.option
	format := make(map[string]interface{})
	format["YmdHis"] = "2006-01-02 15:04:05"
	switch option {
	case "YmdHis":
		return time.Unix(num, 0).Format("2006-01-02 15:04:05")
	case "YmdHi":
		return time.Unix(num, 0).Format("2006-01-02 15:04")
	case "Ymd":
		return time.Unix(num, 0).Format("2006-01-02")
	case "ANSIC":
		return time.Unix(num, 0).Format("Mon Jan _2 15:04:05 2006")
	case "UnixDate":
		return time.Unix(num, 0).Format("Mon Jan _2 15:04:05 MST 2006")
	case "RFC822Z":
		return time.Unix(num, 0).Format("02 Jan 06 15:04 -0700")
	case "RFC850":
		return time.Unix(num, 0).Format("Monday, 02-Jan-06 15:04:05 MST")
	case "RFC1123":
		return time.Unix(num, 0).Format("Mon, 02 Jan 2006 15:04:05 MST")
	case "RFC1123Z":
		return time.Unix(num, 0).Format("Mon, 02 Jan 2006 15:04:05 -0700")
	case "RFC3339":
		return time.Unix(num, 0).Format("2006-01-02T15:04:05Z07:00")
	case "RFC3339Nano":
		return time.Unix(num, 0).Format("2006-01-02T15:04:05.999999999Z07:00")
	case "Kitchen":
		return time.Unix(num, 0).Format("3:04PM")
	case "Stamp":
		return time.Unix(num, 0).Format("Jan _2 15:04:05")
	case "StampMilli":
		return time.Unix(num, 0).Format("Jan _2 15:04:05.000")
	case "StampMicro":
		return time.Unix(num, 0).Format("Jan _2 15:04:05.000000")
	case "StampNano":
		return time.Unix(num, 0).Format("Jan _2 15:04:05.000000000")
	default:
		return time.Unix(num, 0).Format("2006-01-02 15:04:05")
	}

}

////调用
//timeP := new(helper.TimeNow)
//times := timeP.GetTimeStrOrStamp(int64(1483686245), "YmdHis")
//times := timeP.GetTimeStrOrStamp(“2019-07-06 15:04:05”, "YmdHis")
