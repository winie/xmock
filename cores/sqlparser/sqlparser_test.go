package sqlparser

import (
	"bytes"
	"log"
	"testing"

	"github.com/k0kubun/pp"

	"github.com/akito0107/xsqlparser"
	"github.com/akito0107/xsqlparser/dialect"
	"github.com/akito0107/xsqlparser/sqlast"
)

func TestSqlScript_Load(t *testing.T) {
	src := `SELECT os.product, SUM(os.quantity) AS product_units, accounts.* " +
	"FROM orders  os LEFT JOIN accounts ON os.account_id = accounts.id " +
	"WHERE os.region IN (SELECT region FROM top_regions) " +
	"ORDER BY product_units LIMIT 100`

	parser, err := xsqlparser.NewParser(bytes.NewBufferString(src), &dialect.GenericSQLDialect{})
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := parser.ParseStatement()
	if err != nil {
		log.Fatal(err)
	}
	var list []sqlast.Node

	sqlast.Inspect(stmt, func(node sqlast.Node) bool {
		switch node.(type) {
		case nil:
			return false
		default:
			list = append(list, node)
			return true
		}
	})
	pp.Println(list)
}
