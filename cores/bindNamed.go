package cores

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"reflect"
	"strconv"
	"unicode"
	"xmock/cores/utils"
)

// Bindvar types supported by Rebind, BindMap and BindStruct.
const (
	UNKNOWN = iota
	QUESTION
	DOLLAR
	NAMED
)

// 代码来源sqlx
func BindNamedMapper(bindType int, query string, arg interface{}) (string, []interface{}, error) {
	if maparg, ok := arg.(map[string]interface{}); ok {
		return bindMap(bindType, query, maparg)
	}
	return "", nil, errors.New("参数非map类型")
}

// -- Compilation of Named Queries

// Allow digits and letters in bind params;  additionally runes are
// checked against underscores, meaning that bind params can have be
// alphanumeric with underscores.  Mind the difference between unicode
// digits and numbers, where '5' is a digit but '五' is not.
var allowedBindRunes = []*unicode.RangeTable{unicode.Letter, unicode.Digit}

// FIXME: this function isn't safe for unicode named params, as a failing test
// can testify.  This is not a regression but a failure of the original code
// as well.  It should be modified to range over runes in a string rather than
// bytes, even though this is less convenient and slower.  Hopefully the
// addition of the prepared NamedStmt (which will only do this once) will make
// up for the slightly slower ad-hoc NamedExec/NamedQuery.

// compile a NamedQuery into an unbound query (using the '?' bindvar) and
// a list of names.
func compileNamedQuery(qs []byte, bindType int) (query string, names []string, err error) {
	names = make([]string, 0, 10)
	rebound := make([]byte, 0, len(qs))

	inName := false
	last := len(qs) - 1
	currentVar := 1
	name := make([]byte, 0, 10)

	for i, b := range qs {
		// a ':' while we're in a name is an error
		if b == ':' {
			// if this is the second ':' in a '::' escape sequence, append a ':'
			if inName && i > 0 && qs[i-1] == ':' {
				rebound = append(rebound, ':')
				inName = false
				continue
			} else if inName {
				err = errors.New("unexpected `:` while reading named param at " + strconv.Itoa(i))
				return query, names, err
			}
			inName = true
			name = []byte{}
			// if we're in a name, and this is an allowed character, continue
		} else if inName && (unicode.IsOneOf(allowedBindRunes, rune(b)) || b == '_' || b == '.') && i != last {
			// append the byte to the name if we are in a name and not on the last byte
			name = append(name, b)
			// if we're in a name and it's not an allowed character, the name is done
		} else if inName {
			inName = false
			// if this is the final byte of the string and it is part of the name, then
			// make sure to add it to the name
			if i == last && unicode.IsOneOf(allowedBindRunes, rune(b)) {
				name = append(name, b)
			}
			// add the string representation to the names list
			names = append(names, string(name))
			// add a proper bindvar for the bindType
			switch bindType {
			// oracle only supports named type bind vars even for positional
			case NAMED:
				rebound = append(rebound, ':')
				rebound = append(rebound, name...)
			case QUESTION, UNKNOWN:
				rebound = append(rebound, '?')
			case DOLLAR:
				rebound = append(rebound, '$')
				for _, b := range strconv.Itoa(currentVar) {
					rebound = append(rebound, byte(b))
				}
				currentVar++
			}
			// add this byte to string unless it was not part of the name
			if i != last {
				rebound = append(rebound, b)
			} else if !unicode.IsOneOf(allowedBindRunes, rune(b)) {
				rebound = append(rebound, b)
			}
		} else {
			// this is a normal byte and should just go onto the rebound query
			rebound = append(rebound, b)
		}
	}

	return string(rebound), names, err
}

// like bindArgs, but for maps.
func bindMapArgs(names []string, arg map[string]interface{}) ([]interface{}, error) {
	arglist := make([]interface{}, 0, len(names))

	for _, name := range names {
		val, ok := arg[name]
		if !ok {
			return arglist, fmt.Errorf("could not find name %s in %#v", name, arg)
		}
		arglist = append(arglist, val)
	}
	return arglist, nil
}

// bindMap binds a named parameter query with a map of arguments.
func bindMap(bindType int, query string, args map[string]interface{}) (string, []interface{}, error) {
	bound, names, err := compileNamedQuery([]byte(query), bindType)
	if err != nil {
		return "", []interface{}{}, err
	}
	arglist, err := bindMapArgs(names, args)
	return bound, arglist, err
}

func GetSql(mapInfo map[string]string, templateC string) string {
	buf := new(bytes.Buffer)
	//new 模板，命名temHtml
	temEmailHtml, _ := template.New(string(utils.SnowFlakeId())).Parse(templateC)
	//将mapInfo信息添加到模板，结果存入buf
	//buf默认为输入流对象，默认输出到终端
	temEmailHtml.Execute(buf, mapInfo)
	return buf.String()
}

func ParsedSql(mapInfo map[string]interface{}, templateC string) string {
	buf := new(bytes.Buffer)
	//new 模板，命名temHtml
	temEmailHtml, _ := template.New(string(utils.SnowFlakeId())).Parse(templateC)
	//将mapInfo信息添加到模板，结果存入buf
	//buf默认为输入流对象，默认输出到终端
	temEmailHtml.Execute(buf, mapInfo)
	return buf.String()
}

// isEmpty gets whether the specified object is considered empty or not.
func SqlInParam(object interface{}) (string, []interface{}) {
	param := make([]interface{}, 0, 0)
	var sql string
	// get nil case out of the way
	if object == nil {
		return sql, param
	}
	listKind := reflect.TypeOf(object).Kind()
	listValue := reflect.ValueOf(object)
	if listKind == reflect.String || listKind == reflect.Int64 || listKind == reflect.Int || listKind == reflect.Int8 || listKind == reflect.Int16 || listKind == reflect.Int32 {
		sql = sql + `?`
		param = append(param, object)
		return sql, param
	}
	for i := 0; i < listValue.Len(); i++ {
		sql = sql + `?`
		if i+1 <= listValue.Len()-1 {
			sql = sql + `,`
		}
		param = append(param, listValue.Index(i).Interface())
	}
	return sql, param
}
