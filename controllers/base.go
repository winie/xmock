package controllers

import (
	"strconv"
	"strings"
	"xmock/cores/utils"

	"github.com/beego/beego/v2/core/logs"
	beego "github.com/beego/beego/v2/server/web"
)

type BaseController struct {
	beego.Controller
	IsLogin      bool
	UserUserId   int64
	UserUsername string
	UserAvatar   string
}

func (this *BaseController) Prepare() {
	user := this.GetSession("userLogin")
	if !utils.IsEmpty(user) {
		tmp := strings.Split(user.(string), "||")
		if tmp != nil {
			logs.Info("登陆:", tmp)
			userid, _ := strconv.Atoi(tmp[0])
			longid := int64(userid)
			this.UserUserId = longid
			this.UserUsername = tmp[1]
		}
	}
}
