// @APIVersion 1.0.0
// @Title xmock API
// @Description xmock 后端接口
// @Contact
// @TermsOfServiceUrl
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"xmock/app/auth"
	"xmock/app/dict"
	"xmock/app/file"
	"xmock/app/form"
	"xmock/filter"

	beego "github.com/beego/beego/v2/server/web"
)

func init() {
	StaticPath, _ := beego.AppConfig.String("StaticPath")
	beego.InsertFilter("*", beego.BeforeRouter, filter.CorsHandler())
	beego.InsertFilter("/*", beego.BeforeRouter, filter.MockHandler())
	// 增加xmock 代表独特api 接口以免和其他业务接口有重叠
	beego.SetStaticPath("/api/xv1/file", StaticPath)
	beego.InsertFilter("/api/xv1/*", beego.BeforeRouter, filter.Authorizer())
	ns := beego.NewNamespace("/api/xv1",
		beego.NSNamespace("/upload",
			beego.NSInclude(
				&file.UploadFileController{},
			),
		),
		beego.NSNamespace("/dict",
			beego.NSInclude(
				&dict.DictController{},
			),
		),
		beego.NSRouter("/login", &auth.LoginController{}, "post:Login"),
		beego.NSNamespace("/user",
			beego.NSRouter("/", &auth.UserController{}, "post:CreateUserCtl"),
			beego.NSRouter("/:id", &auth.UserController{}, "delete:DeleteUserCtl"),
			beego.NSRouter("/:id", &auth.UserController{}, "put:UpdateUserCtl"),
			beego.NSRouter("/:account", &auth.UserController{}, "get:FindUserByAccountCtl"),
			beego.NSRouter("/:id/role", &auth.UserRoleController{}, "post:UserDistributorRoleCtl"),
			beego.NSRouter("/:id/role", &auth.UserRoleController{}, "get:RoleByUserIdCtl"),
			beego.NSRouter("/", &auth.UserController{}, "get:FindUserByPageCtl"),
		),
		beego.NSNamespace("/role",
			beego.NSRouter("/", &auth.RoleController{}, "post:CreateRoleCtl"),
			beego.NSRouter("/:id", &auth.RoleController{}, "delete:DeleteRoleCtl"),
			beego.NSRouter("/:id", &auth.RoleController{}, "put:UpdateRoleCtl"),
			beego.NSRouter("/:code", &auth.RoleController{}, "get:FindRoleByCodeCtl"),
			beego.NSRouter("/:id/resource", &auth.RoleResourceController{}, "post:RoleDistributorResourceCtl"),
			beego.NSRouter("/:id/resource", &auth.RoleResourceController{}, "get:ResourceByRoleIdCtl"),
			beego.NSRouter("/", &auth.RoleController{}, "get:FindRoleByPageCtl"),
		),
		beego.NSNamespace("/resource",
			beego.NSRouter("/", &auth.ResourceController{}, "post:CreateResourceCtl"),
			beego.NSRouter("/:id", &auth.ResourceController{}, "delete:DeleteResourceCtl"),
			beego.NSRouter("/:id", &auth.ResourceController{}, "put:UpdateResourceCtl"),
			beego.NSRouter("/:code", &auth.ResourceController{}, "get:FindResourceByCodeCtl"),
			beego.NSRouter("/", &auth.ResourceController{}, "get:FindResourceByPageCtl"),
			beego.NSRouter("/:userId", &auth.ResourceController{}, "get:MenusByUserId"),
		),

		beego.NSNamespace("/menus",
			beego.NSRouter("/:userId", &auth.ResourceController{}, "get:MenusByUserId"),
		),
		beego.NSNamespace("/client",
			beego.NSRouter("/", &auth.ClientController{}, "post:CreateClientCtl"),
			beego.NSRouter("/", &auth.ClientController{}, "get:FindClientByPageCtl"),
			beego.NSRouter("/:id", &auth.ClientController{}, "delete:DeleteClientCtl"),
			beego.NSRouter("/:id", &auth.ClientController{}, "put:UpdateClientCtl"),
			beego.NSRouter("/:clientId", &auth.ClientController{}, "get:FindClientByClientIdCtl"),
		),
		beego.NSNamespace("/app",
			beego.NSRouter("/", &auth.ClientController{}, "get:FindClientDefault"),
		),
		beego.NSNamespace("/form",
			beego.NSRouter("/page", &form.FormController{}, "get:FindByPage"),
			beego.NSRouter("/", &form.FormController{}, "post:Create"),
			beego.NSRouter("/", &form.FormController{}, "put:Update"),
			beego.NSRouter("/publish", &form.FormController{}, "put:Publish"),
		),
	)
	beego.AddNamespace(ns)
}
