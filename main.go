package main

import (
	_ "xmock/app/initial"
	_ "xmock/routers"

	beego "github.com/beego/beego/v2/server/web"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.BConfig.WebConfig.ViewsPath = "webs"
	beego.BConfig.WebConfig.AutoRender = false
	beego.BConfig.WebConfig.CommentRouterPath = "app"
	beego.Run()
}
